<?php

namespace Drupal\media_library\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\SortArray;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\media\Entity\Media;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Plugin implementation of the 'media_library_widget' widget.
 *
 * @todo We don't support the "auto_create", "auto_create_bundle", or
 * "handler" field settings. Determine how to document or address this.
 *
 * @FieldWidget(
 *   id = "media_library_widget",
 *   label = @Translation("Media Library"),
 *   description = @Translation("Allows you to re-use Media Entities."),
 *   field_types = {
 *     "entity_reference"
 *   },
 *   multiple_values = TRUE
 * )
 */
class MediaLibraryWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a MediaLibraryWidget widget.
   *
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getSetting('target_type') === 'media';
  }

  /**
   * {@inheritdoc}
   */
  public function form(FieldItemListInterface $items, array &$form, FormStateInterface $form_state, $get_delta = NULL) {
    $field_name = $this->fieldDefinition->getName();
    $parents = $form['#parents'];

    // Load the items for form rebuilds from the field state as they might not be
    // in $form_state['values'] because of validation limitations.
    $field_state = static::getWidgetState($parents, $field_name, $form_state);
    if ($field_state && isset($field_state['items'])) {
      usort($field_state['items'], [SortArray::class, 'sortByWeightElement']);
      $items->setValue($field_state['items']);
    }

    $build = parent::form($items, $form, $form_state, $get_delta);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
    $referenced_entities = $items->referencedEntities();
    $view_builder = $this->entityTypeManager->getViewBuilder($this->getFieldSetting('target_type'));
    $field_name = $this->fieldDefinition->getName();
    $wrapper_id = $field_name . '-media-library-wrapper';
    $parents = $form['#parents'];

    $element += [
      '#type' => 'fieldset',
      '#cardinality' => $this->fieldDefinition->getFieldStorageDefinition()->getCardinality(),
      'selection' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => 'media-library-selection',
        ],
      ],
      '#attributes' => [
        'id' => $wrapper_id,
        'class' => ['media-library-wrapper'],
      ],
    ];

    if (empty($referenced_entities)) {
      $element['empty_selection'] = [
        '#markup' => $this->t('<p>No media selected.</p>'
      )];
    }
    else {
      foreach ($referenced_entities as $delta => $media_item) {
        $element['selection'][$delta] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => ['media-library-item', 'media-library-item-in-widget'],
          ],
          'preview' => [
            '#type' => 'container',
            'view' => $view_builder->view($media_item, 'media_library'),
            '#attached' => [
              'library' => ['media_library/widget'],
            ],
            'remove_button' => [
              '#type' => 'submit',
              '#media_library_remove_delta' =>$delta,
              '#name' => $field_name . '-' .  $delta . '-media-library-remove-button',
              '#value' => $this->t('Remove'),
              '#attributes' => [
                'class' => ['media-library-item-remove'],
              ],
              '#ajax' => [
                'callback' => [static::class, 'updateWidget'],
                'wrapper' => $wrapper_id,
              ],
              '#submit' => [[static::class, 'removeItem']],
              '#limit_validation_errors' => [array_merge($form['#parents'], [$field_name])],
            ],
          ],
          'target_id' => [
            '#type' => 'hidden',
            '#value' => $media_item->id(),
          ],
          // This hidden value is set by JS when items are re-sorted.
          'weight' => [
            '#type' => 'hidden',
            '#default_value' => $delta,
            '#attributes' => [
              'class' => ['media-library-item-weight'],
            ],
          ],
        ];
      }
    }

    // @todo We use a use-ajax link here as it is the only way we've seen
    // contextual filters work when using a view in a modal. Unfortunately
    // this only works the first time you open the modal - opening it twice
    // in one page load breaks Views AJAX.
    $element['media_library_open_container'] = [
      '#type' => 'container',
      'media_library_open_button' => [
        '#type' => 'link',
        '#title' => $this->t('Add Media'),
        '#name' => $field_name . '-media-library-open-button',
        '#url' => Url::fromRoute('view.media_library.widget', [], [
          'query' => ['media_library_input' => $field_name],
        ]),
        '#attributes' => [
          'class' => ['button', 'use-ajax', 'media-library-open-button'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => json_encode([
            'height' => '500',
            'width' => '70%',
            'title' => $this->t('Media library'),
          ]),
        ],
        '#limit_validation_errors' => [array_merge($parents, [$field_name])],
      ],
    ];

    // Add a hidden textfield to the form, which is used by the select_media
    // Views field to pass values from the modal to the widget. This is similar
    // to how the EntityReferenceAutocompleteWidget stores values.
    $element['media_library_selection'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'data-media-library-input-id' => $field_name,
        'class' => ['visually-hidden'],
      ],
    ];

    $element['media_library_update_widget'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update widget'),
      '#name' => $field_name . '-media-library-update-button',
      '#ajax' => [
        'callback' => [static::class, 'updateWidget'],
        'wrapper' => $wrapper_id,
      ],
      '#attributes' => [
        'data-media-library-submit-id' => $field_name,
        'class' => ['visually-hidden', 'media-library-update-button'],
      ],
      '#submit' => [[static::class, 'updateItems']],
      '#limit_validation_errors' => [array_merge($parents, [$field_name])],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $error, array $form, FormStateInterface $form_state) {
    return isset($element['target_id']) ? $element['target_id'] : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    if (isset($values['selection'])) {
      usort($values['selection'], [SortArray::class, 'sortByWeightElement']);
      return $values['selection'];
    }
    else {
      return [];
    }
  }

  /**
   * AJAX callback to update the widget when the selection changes.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   An array representing the updated widget.
   */
  public static function updateWidget($form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $length = isset($triggering_element['#media_library_remove_delta']) ? -4 : -1;
    $parents = $triggering_element['#array_parents'];
    $parents = array_slice($parents, 0, $length);
    $element = NestedArray::getValue($form, $parents);
    // Always clear the textfield selection to prevent duplicate additions.
    $element['media_library_selection']['#value'] = '';
    return $element;
  }

  /**
   * Submit callback for remove buttons.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function removeItem($form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();

    $parents = $triggering_element['#array_parents'];
    $parents = array_slice($parents, 0, -4);
    $element = NestedArray::getValue($form, $parents);
    $field_name = $element['#field_name'];
    $parents = $element['#field_parents'];
    $delta = $triggering_element['#media_library_remove_delta'];

    // Find and remove correct entity.
    $path = array_merge($form['#parents'], [$field_name]);
    $values = NestedArray::getValue($form_state->getValues(), $path);
    $field_state = static::getWidgetState($parents, $field_name, $form_state);
    if (isset($values['selection'])) {
      if (isset($values['selection'][$delta])) {
        array_splice($values['selection'], $delta, 1);
        $field_state['items'] = $values['selection'];
        $field_state['items_count'] = count($field_state['items']);
      }
    }
    $form_state->setValue($field_name, $values);
    static::setWidgetState($parents, $field_name, $form_state, $field_state);

    // Rebuild form.
    $form_state->setRebuild();
  }

  /**
   * Updates the field state based on the form state and sets the form rebuild.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function updateItems($form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();

    // Go one level up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -1));
    $field_name = $element['#field_name'];
    $parents = $element['#field_parents'];
    $field_state = static::getWidgetState($parents, $field_name, $form_state);

    // Get the new media ids passed to our hidden button.
    $values = $form_state->getValues();
    $path = array_merge($form['#parents'], [$field_name]);
    $value = NestedArray::getValue($values, $path);
    $selection = isset($value['selection']) ? $value['selection'] : [];

    $field_state['items'] = $selection;

    if (!empty($value['media_library_selection'])) {
      $ids = explode(',', $value['media_library_selection']);
      /** @var \Drupal\media\MediaInterface[] $media */
      $media = Media::loadMultiple($ids);

      // Append the provided Media to the existing selection.
      $cardinality_unlimited = ($element['#cardinality'] === FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
      foreach ($media as $media_item) {
        if ($cardinality_unlimited || (count($field_state['items']) < $element['#cardinality'])) {
          if ($media && $media_item->access('view')) {
            $field_state['items'][] = [
              'target_id' => $media_item->id(),
            ];
          }
        }
      }
    }

    $field_state['items_count'] = count($field_state['items']);
    static::setWidgetState($parents, $field_name, $form_state, $field_state);

    $form_state->setRebuild();
  }

}
