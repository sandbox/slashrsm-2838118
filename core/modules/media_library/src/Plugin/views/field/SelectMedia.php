<?php

/**
 * @file
 * Contains \Drupal\media_library\Plugin\views\field\SelectEntity.
 */

namespace Drupal\media_library\Plugin\views\field;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\system\Plugin\views\field\SelectFormBase;

/**
 * Provides a field for selecting media entities in our media library view.
 *
 * @ViewsField("select_media")
 */
class SelectMedia extends SelectFormBase {

  /**
   * {@inheritdoc}
   */
  public function viewsForm(&$form, FormStateInterface $form_state) {
    parent::viewsForm($form, $form_state);
    if (!empty($form[$this->options['id']])) {
      foreach (Element::children($form[$this->options['id']]) as $index) {
        $item = &$form[$this->options['id']][$index];
        $item['#attributes']['data-media-select'] = TRUE;
      }
    }
    if (isset($form['actions']['submit'])) {
      $form['actions']['submit']['#attributes']['data-media-select-submit'] = TRUE;
      $form['actions']['submit']['#ajax']['url'] = Url::fromRoute('<current>');
      $form['actions']['submit']['#ajax']['callback'] = function ($form, FormStateInterface $form_state) {
        $response = new AjaxResponse();
        $response->addCommand(new CloseModalDialogCommand());
        $form_state->setResponse($response);
      };
    }
    if ($media_library_input = \Drupal::request()->get('media_library_input')) {
      $form['#attached']['drupalSettings']['media_library']['media_library_inputs'][$this->view->dom_id] = $media_library_input;
    }
  }

  /**
   * Submit handler for the bulk form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Thrown when the user tried to access an action without access to it.
   */
  public function viewsFormSubmit(&$form, FormStateInterface $form_state) {
    if ($form_state->get('step') == 'views_form_views_form') {
      // Prefer the js provided selection, because it contains selected entities
      // that aren't currently on screen and tracks the order of the selection.
      if (\Drupal::request()->request->has('selected_entities')) {
        $selected = \Drupal::request()->request->get('selected_entities');
      }
      else {
        // Filter only selected checkboxes.
        $selected = array_filter($form_state->getValue($this->options['id']));
      }

      $entities = [];

      foreach ($selected as $bulk_form_key) {
        $entity = $this->loadEntityFromBulkFormKey($bulk_form_key);
        $entities[$bulk_form_key] = $entity;
      }

      $ids = [];
      foreach ($entities as $entity) {
        $ids[] = $entity->id();
      }

      $response = new AjaxResponse();
      $library_input = \Drupal::request()->get('media_library_input');
      $input_selector = '[data-media-library-input-id="' . $library_input  . '"]';
      $submit_selector = '[data-media-library-submit-id="' . $library_input  . '"]';
      $response->addCommand(new InvokeCommand($input_selector, 'val', [implode(',', $ids)]));
      $response->addCommand(new InvokeCommand($submit_selector, 'trigger', ['mousedown']));
      $response->addCommand(new CloseModalDialogCommand());
      $form_state->setResponse($response);
    }
  }

}
