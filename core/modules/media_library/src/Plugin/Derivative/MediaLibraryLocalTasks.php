<?php

namespace Drupal\media_library\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\media\Entity\MediaType;

/**
 * Defines dynamic local tasks for the Media Library module.
 */
class MediaLibraryLocalTasks extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $weight = 0;
    foreach (MediaType::loadMultiple() as $id => $bundle) {
      $this->derivatives[$id] = $base_plugin_definition;
      $this->derivatives[$id]['title'] = $bundle->label();
      $this->derivatives[$id]['route_name'] = 'view.media_library.bundle_page';
      $this->derivatives[$id]['parent_id'] = 'views_view:view.media_library.page';
      $this->derivatives[$id]['weight'] = ++$weight;
      $this->derivatives[$id]['route_parameters'] = ['bundle' => $id];
    }
    return $this->derivatives;
  }

}
