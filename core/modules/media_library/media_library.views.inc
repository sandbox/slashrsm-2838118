<?php

/**
 * Implements hook_views_data().
 */
function media_library_views_data() {
  $data = [];
  // Registers an library select field per entity.
  foreach (\Drupal::entityTypeManager()->getDefinitions() as $entity_type => $entity_info) {
    $data[$entity_info->getBaseTable()][$entity_type . '_select_media'] = array(
      'title' => t('Select media'),
      'help' => t('Provides a field for selecting media entities in our media library view'),
      'real field' => $entity_info->getKey('id'),
      'field' => array(
        'id' => 'select_media',
      ),
    );
  }
  return $data;
}
