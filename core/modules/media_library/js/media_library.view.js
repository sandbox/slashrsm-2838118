/**
 * @file media_library.view.js
 */

(function ($, Drupal) {

  "use strict";

  // Locally-scoped variable that stores the current View selection. We track
  // this in a variable as users may click a media item and then re-load the
  // View with AJAX (filters, pagers, sorts, etc.), which will lose what is
  // currently selected in the bulk form. This could likely be back-ported to
  // core.
  var MediaLibrarySelectedEntities = {};

  /**
   * Allows users to click on hidden Views select checkboxes by clicking on the
   * media item (which is also a Views row).
   */
  Drupal.behaviors.MediaLibraryCheckboxes = {
    attach: function (context) {
      $('.media-library-item', context).once('media-library-checkbox').on('click', function (event) {
        // Links inside the rendered media item should not be click-able.
        event.preventDefault();
        // Click the hidden checkbox when the media item is clicked.
        var $input = $(this).find('.media-library-item-checkbox input');
        $input.prop('checked', !$input.prop('checked'));
        if ($input.prop('checked')) {
          $(this).addClass('checked');
        }
        else {
          $(this).removeClass('checked');
        }
        $input.trigger('change');
      });
    }
  };

  /**
   * Persists bulk and select form selections across AJAX View re-renders.
   */
  Drupal.behaviors.MediaLibraryPersistentSelection = {
    attach: function (context) {
      // For each View on the page, store the dom-id as a data attribute so
      // that we can easily access it later. This is normally only stored in a
      // class.
      $('[class*="js-view-dom-id-"]', context).once('set-dom-id-data-attribute').each(function () {
        var element = $(this);
        var classes = $(this).attr('class').split(/\s+/);
        $(classes).each(function (i, class_name) {
          var dom_id = class_name.match(new RegExp('^js-view-dom-id-(.*)$'));
          if (dom_id) {
            element.attr('data-views-dom-id', dom_id[1]);
            if (!MediaLibrarySelectedEntities[dom_id[1]]) {
              MediaLibrarySelectedEntities[dom_id[1]] = [];
            }
          }
        });
      });

      // Check/uncheck checkboxes in the view based on the selected media.
      $('.media-library-item-checkbox input', context).once('data-media-select').each(function (i, element) {
        $(this).on('change', function () {
          var checked = $(this).prop('checked');
          var views_dom_id = $(this).closest('[data-views-dom-id]').data('views-dom-id');
          var entity = $(this).val();
          if (checked) {
            MediaLibrarySelectedEntities[views_dom_id].push(entity);
          }
          else {
            MediaLibrarySelectedEntities[views_dom_id] = $(MediaLibrarySelectedEntities[views_dom_id]).filter(function (i, value) {
              return !(entity == value);
            });
          }
        });
      });
    }
  };

  /**
   * Attaches extra POST data when the Views bulk form is submitted, to ensure
   * that selections that may be off-screen persist during the submit process.
   */
  Drupal.behaviors.MediaLibraryViewsSubmit = {
    attach: function (context, settings) {
      $('[data-views-dom-id]', context).once('media-library-checkbox-submit').each(function () {
        var $view = $(this);
        var views_dom_id = $(this).data('views-dom-id');
        $.each(MediaLibrarySelectedEntities[views_dom_id], function(index, items) {
          $view.find('input[value="' + items + '"]').prop('checked', true);
          $view.find('input[value="' + items + '"]').closest('.media-library-item').addClass('checked');
        });

        // Propagate all selected entities to the form submission.
        $view.find('[data-media-select-submit]').each(function () {
          var button = $(this);
          $.each(Drupal.ajax.instances, function () {
            if (this && $(this.element).is(button)) {
              if (settings && settings.views && settings.views.ajaxViews) {
                var ajaxViews = settings.views.ajaxViews;
                for (var i in ajaxViews) {
                  var ajaxViewsInstance = Drupal.views.instances[i];
                  if (ajaxViewsInstance.settings.view_dom_id == views_dom_id) {
                    this.submit = $.extend(this.submit, ajaxViewsInstance.settings, {
                      selected_entities: MediaLibrarySelectedEntities[views_dom_id],
                      media_library_input: settings.media_library.media_library_inputs[views_dom_id]
                    });
                  }
                }
              }
            }
          });
        });
      });
    }
  };

})(jQuery, Drupal);
