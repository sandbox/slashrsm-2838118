/**
 * @file media_library.widget.js
 */

(function ($, Drupal) {

  "use strict";

  /**
   * Provides custom JS behaviors related to the media library field widget.
   */
  Drupal.behaviors.MediaLibraryWidget = {
    attach: function (context) {
      $('.media-library-selection', context).sortable({
        tolerance: 'pointer',
        stop: function(event, ui) {
          // Update all the hidden "weight" fields.
          $(event.target).children().each(function (index) {
            $(this).find('.media-library-item-weight').val(index);
          });
        }
      });
    }
  };

})(jQuery, Drupal);
