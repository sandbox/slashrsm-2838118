<?php

/**
 * @file
 * Hooks related to Media and its plugins.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the information provided in \Drupal\media\Annotation\MediaHandler.
 *
 * @param array $handlers
 *   The array of handlers plugin definitions, keyed by plugin ID.
 */
function hook_media_handler_info_alter(array &$handlers) {
  $handlers['youtube']['label'] = t('Youtube rocks!');
}

/**
 * @} End of "addtogroup hooks".
 */
