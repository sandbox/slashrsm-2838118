<?php

/**
 * @file
 * Theme functions for the media module.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for media templates.
 *
 * Default template: media.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - media: An individual media for display.
 */
function template_preprocess_media(array &$variables) {
  $variables['media'] = $variables['elements']['#media'];
  $variables['view_mode'] = $variables['elements']['#view_mode'];
  $variables['name'] = $variables['media']->label();

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
