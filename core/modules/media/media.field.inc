<?php

/**
 * @file
 * Field module functionality for the Media module.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for multi file form widget templates.
 *
 * Default template: file-widget-multiple.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: A render element representing the widgets.
 */
function template_preprocess_media_file_widget_multiple(&$variables) {
  $element = $variables['element'];

  // Special ID and classes for draggable tables.
  $weight_class = $element['#id'] . '-weight';
  $table_id = $element['#id'] . '-table';

  // Build up a table of applicable fields.
  $headers = array();
  $headers[] = t('File information');
  if ($element['#display_field']) {
    $headers[] = array(
      'data' => t('Display'),
      'class' => array('checkbox'),
    );
  }
  $headers[] = t('Weight');
  $headers[] = t('Operations');

  // Get our list of widgets in order (needed when the form comes back after
  // preview or failed validation).
  $widgets = array();
  foreach (Element::children($element['selection']) as $key) {
    $widgets[] = &$element['selection'][$key];
  }
  usort($widgets, '_field_multiple_value_form_sort_helper');

  $rows = array();
  foreach ($widgets as $key => &$widget) {
    // Save the uploading row for last.
    if (empty($widget['file_field'])) {
      $widget['#title'] = $element['#file_upload_title'];
      $widget['#description'] = \Drupal::service('renderer')->renderPlain($element['#file_upload_description']);
      continue;
    }

    $file_field = &$widget['file_field'];

    // Delay rendering of the buttons, so that they can be rendered later in the
    // "operations" column.
    $operations_elements = array();
    foreach (Element::children($file_field) as $sub_key) {
      if (isset($file_field[$sub_key]['#type']) && $file_field[$sub_key]['#type'] == 'submit') {
        hide($file_field[$sub_key]);
        $operations_elements[] = &$file_field[$sub_key];
      }
    }

    // Delay rendering the weight selector, so that
    // it can be rendered later in its own column.
    hide($widget['_weight']);

    // Render everything else together in a column, without the normal wrappers.
    $file_field['#theme_wrappers'] = array();
    $information = \Drupal::service('renderer')->render($file_field);

    $widget['_weight']['#attributes']['class'] = array($weight_class);
    $weight = render($widget['_weight']);

    // Arrange the row with all of the rendered columns.
    $row = array();
    $row[] = $information;
    $row[] = $weight;

    // Show the buttons that had previously been marked as hidden in this
    // preprocess function. We use show() to undo the earlier hide().
    foreach (Element::children($operations_elements) as $key) {
      show($operations_elements[$key]);
    }
    $row[] = array(
      'data' => $operations_elements,
    );
    $rows[] = array(
      'data' => $row,
      'class' => isset($file_field['#attributes']['class']) ? array_merge($file_field['#attributes']['class'], array('draggable')) : array('draggable'),
    );
  }

  $variables['table'] = array(
    '#type' => 'table',
    '#header' => $headers,
    '#rows' => $rows,
    '#attributes' => array(
      'id' => $table_id,
    ),
    '#tabledrag' => array(
      array(
        'action' => 'order',
        'relationship' => 'sibling',
        'group' => $weight_class,
      ),
    ),
    '#access' => !empty($rows),
  );

  $variables['element'] = $element;
}
