<?php

/**
 * @file
 * Builds placeholder replacement tokens for media-related data.
 */

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function media_token_info() {
  $type = [
    'name' => t('Media'),
    'description' => t('Tokens related to individual media items.'),
    'needs-data' => 'media',
  ];

  // Core tokens for media.
  $media['mid'] = [
    'name' => t('Media ID'),
    'description' => t('The unique ID of the media item.'),
  ];
  $media['uuid'] = [
    'name' => t('Media UUID'),
    'description' => t('The unique UUID of the media item.'),
  ];
  $media['vid'] = [
    'name' => t('Revision ID'),
    'description' => t("'The unique ID of the media's latest revision."),
  ];
  $media['bundle'] = [
    'name' => t('Media type'),
  ];
  $media['bundle-name'] = [
    'name' => t('Media type name'),
    'description' => t('The human-readable name of the media type.'),
  ];
  $media['langcode'] = [
    'name' => t('Language code'),
    'description' => t('The language code of the language the media is written in.'),
  ];
  $media['name'] = [
    'name' => t('Name'),
    'description' => t('The name of this media.'),
  ];
  $media['author'] = [
    'name' => t('Author'),
    'type' => 'user',
  ];
  $media['url'] = [
    'name' => t('URL'),
    'description' => t('The URL of the media.'),
  ];
  $media['edit-url'] = [
    'name' => t('Edit URL'),
    'description' => t("The URL of the media's edit page."),
  ];

  // Chained tokens for media.
  $media['created'] = [
    'name' => t('Date created'),
    'type' => 'date',
  ];
  $media['changed'] = [
    'name' => t('Date changed'),
    'description' => t('The date the media was most recently updated.'),
    'type' => 'date',
  ];

  return [
    'types' => ['media' => $type],
    'tokens' => ['media' => $media],
  ];
}

/**
 * Implements hook_tokens().
 */
function media_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $token_service = \Drupal::token();

  $url_options = ['absolute' => TRUE];
  if (isset($options['langcode'])) {
    $url_options['language'] = \Drupal::languageManager()->getLanguage($options['langcode']);
    $langcode = $options['langcode'];
  }
  else {
    $langcode = LanguageInterface::LANGCODE_DEFAULT;
  }

  $replacements = [];
  if ($type == 'media' && !empty($data['media'])) {
    /** @var \Drupal\media\MediaInterface $media */
    $media = \Drupal::service('entity.repository')->getTranslationFromContext($data['media'], $langcode, ['operation' => 'media_entity_tokens']);

    foreach ($tokens as $name => $original) {
      switch ($name) {
        // Simple key values on the media_entity.
        case 'mid':
          $replacements[$original] = $media->id();
          break;

        case 'uuid':
          $replacements[$original] = $media->uuid();
          break;

        case 'vid':
          $replacements[$original] = $media->getRevisionId();
          break;

        case 'bundle':
          $replacements[$original] = $media->bundle();
          break;

        case 'bundle-name':
          $replacements[$original] = $media->bundle->entity->label();
          break;

        case 'langcode':
          $replacements[$original] = $media->language()->getId();
          break;

        case 'name':
          $replacements[$original] = $media->label();
          break;

        case 'url':
          $replacements[$original] = $media->toUrl('canonical', $url_options);
          break;

        case 'edit-url':
          $replacements[$original] = $media->toUrl('edit-form', $url_options);
          break;

        // Default values for the chained tokens handled below.
        case 'author':
          $account = $media->getOwner();
          $bubbleable_metadata->addCacheableDependency($account);
          $replacements[$original] = $account->label();
          break;

        case 'created':
          $date_format = DateFormat::load('medium');
          $bubbleable_metadata->addCacheableDependency($date_format);
          $replacements[$original] = \Drupal::service('date.formatter')
            ->format($media->getCreatedTime(), 'medium', '', NULL, $langcode);
          break;

        case 'changed':
          $date_format = DateFormat::load('medium');
          $bubbleable_metadata->addCacheableDependency($date_format);
          $replacements[$original] = \Drupal::service('date.formatter')
            ->format($media->getChangedTime(), 'medium', '', NULL, $langcode);
          break;
      }
    }

    if ($author_tokens = $token_service->findWithPrefix($tokens, 'author')) {
      $account = $media->get('uid')->entity;
      $replacements += $token_service->generate('user', $author_tokens, ['user' => $account], $options, $bubbleable_metadata);
    }

    if ($created_tokens = $token_service->findWithPrefix($tokens, 'created')) {
      $replacements += $token_service->generate('date', $created_tokens, ['date' => $media->getCreatedTime()], $options, $bubbleable_metadata);
    }

    if ($changed_tokens = $token_service->findWithPrefix($tokens, 'changed')) {
      $replacements += $token_service->generate('date', $changed_tokens, ['date' => $media->getChangedTime()], $options, $bubbleable_metadata);
    }
  }

  return $replacements;
}
