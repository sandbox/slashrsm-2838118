<?php

namespace Drupal\Tests\media\FunctionalJavascript;

use Drupal\media\Entity\Media;

/**
 * Ensures that media UI works correctly.
 *
 * @group media
 */
class MediaUiJavascriptTest extends MediaJavascriptTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'block',
    'media_test_handler',
  ];

  /**
   * The test media type.
   *
   * @var \Drupal\media\MediaTypeInterface
   */
  protected $testBundle;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('local_tasks_block');
  }

  /**
   * Tests a media bundle administration.
   */
  public function testMediaBundles() {
    $session = $this->getSession();
    $page = $session->getPage();
    $assert_session = $this->assertSession();

    $this->drupalGet('admin/structure/media');
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains('No media types available. Add media type.');
    $assert_session->linkExists('Add media type');

    // Test the creation of a media bundle using the UI.
    $name = $this->randomMachineName();
    $description = $this->randomMachineName();
    $this->drupalGet('admin/structure/media/add');
    $page->fillField('label', $name);
    $machine_name = strtolower($name);
    $this->assertJsCondition("jQuery('.machine-name-value').html() == '$machine_name'");
    $page->selectFieldOption('handler', 'test');
    $this->assertJsCondition("jQuery('.form-item-handler-configuration-test-config-value').length > 0;");
    $page->fillField('description', $description);
    $page->pressButton('Save');
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains('The media type ' . $name . ' has been added.');
    $this->drupalGet('admin/structure/media');
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains($name);
    $assert_session->pageTextContains($description);

    /** @var \Drupal\media\MediaTypeInterface $bundle_storage */
    $bundle_storage = $this->container->get('entity_type.manager')->getStorage('media_type');
    $this->testBundle = $bundle_storage->load(strtolower($name));

    // Check if all action links exist.
    $assert_session->linkByHrefExists('admin/structure/media/add');
    $assert_session->linkByHrefExists('admin/structure/media/manage/' . $this->testBundle->id());
    $assert_session->linkByHrefExists('admin/structure/media/manage/' . $this->testBundle->id() . '/fields');
    $assert_session->linkByHrefExists('admin/structure/media/manage/' . $this->testBundle->id() . '/form-display');
    $assert_session->linkByHrefExists('admin/structure/media/manage/' . $this->testBundle->id() . '/display');

    // Assert that fields have expected values before editing.
    $page->clickLink('Edit');
    $assert_session->fieldValueEquals('label', $name);
    $assert_session->fieldValueEquals('description', $description);
    $assert_session->fieldValueEquals('handler', 'test');
    $assert_session->fieldValueEquals('label', $name);
    $assert_session->checkboxNotChecked('edit-options-new-revision');
    $assert_session->checkboxChecked('edit-options-status');
    $assert_session->checkboxNotChecked('edit-options-queue-thumbnail-downloads');
    $assert_session->pageTextContains('Create new revision');
    $assert_session->pageTextContains('Automatically create new revisions. Users with the Administer media permission will be able to override this option.');
    $assert_session->pageTextContains('Download thumbnails via a queue.');
    $assert_session->pageTextContains('Media will be automatically published when created.');
    $assert_session->pageTextContains('Media handlers can provide metadata fields such as title, caption, size information, credits, etc. Media can automatically save this metadata information to entity fields, which can be configured below. Information will only be mapped if the entity field is empty.');

    // Try to change media type and check if new configuration sub-form appears.
    $page->selectFieldOption('handler', 'test');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->fieldExists('Test config value');
    $assert_session->fieldValueEquals('Test config value', 'This is default value.');
    $assert_session->fieldExists('Field 1');
    $assert_session->fieldExists('Field 2');

    // Test if the edit machine name is not editable.
    $assert_session->fieldDisabled('Machine-readable name');

    // Edit and save media bundle form fields with new values.
    $new_name = $this->randomMachineName();
    $new_description = $this->randomMachineName();
    $page->fillField('label', $new_name);
    $page->fillField('description', $new_description);
    $page->selectFieldOption('handler', 'test');
    $page->fillField('Test config value', 'This is new config value.');
    $page->selectFieldOption('field_map[field_1]', 'name');
    $page->checkField('options[new_revision]');
    $page->uncheckField('options[status]');
    $page->checkField('options[queue_thumbnail_downloads]');
    $page->pressButton('Save');
    $assert_session->statusCodeEquals(200);

    // Test if edit worked and if new field values have been saved as expected.
    $this->drupalGet('admin/structure/media/manage/' . $this->testBundle->id());
    $assert_session->fieldValueEquals('label', $new_name);
    $assert_session->fieldValueEquals('description', $new_description);
    $assert_session->fieldValueEquals('handler', 'test');
    $assert_session->checkboxChecked('options[new_revision]');
    $assert_session->checkboxNotChecked('options[status]');
    $assert_session->checkboxChecked('options[queue_thumbnail_downloads]');
    $assert_session->fieldValueEquals('Test config value', 'This is new config value.');
    $assert_session->fieldValueEquals('Field 1', 'name');
    $assert_session->fieldValueEquals('Field 2', '_none');

    /** @var \Drupal\media\MediaTypeInterface $loaded_bundle */
    $loaded_bundle = $this->container->get('entity_type.manager')
      ->getStorage('media_type')
      ->load($this->testBundle->id());
    $this->assertEquals($loaded_bundle->id(), $this->testBundle->id());
    $this->assertEquals($loaded_bundle->label(), $new_name);
    $this->assertEquals($loaded_bundle->getDescription(), $new_description);
    $this->assertEquals($loaded_bundle->getHandler()->getPluginId(), 'test');
    $this->assertEquals($loaded_bundle->getHandler()->getConfiguration()['test_config_value'], 'This is new config value.');
    $this->assertTrue($loaded_bundle->shouldCreateNewRevision());
    $this->assertTrue($loaded_bundle->getQueueThumbnailDownloadsStatus());
    $this->assertFalse($loaded_bundle->getStatus());
    $this->assertEquals($loaded_bundle->getFieldMap(), ['field_1' => 'name']);

    // We need to clear the statically cached field definitions to account for
    // fields that have been created by API calls in this test, since they exist
    // in a separate memory space from the web server.
    $this->container->get('entity_field.manager')->clearCachedFieldDefinitions();

    // Test that a media being created with default status to "FALSE" will be
    // created unpublished.
    /** @var \Drupal\media\MediaInterface $unpublished_media */
    $unpublished_media = Media::create(['name' => 'unpublished test media', 'bundle' => $loaded_bundle->id()]);
    $this->assertFalse($unpublished_media->isPublished());
    $unpublished_media->delete();

    // Tests media bundle delete form.
    $page->clickLink('Delete');
    $assert_session->addressEquals('admin/structure/media/manage/' . $this->testBundle->id() . '/delete');
    $page->pressButton('Delete');
    $assert_session->addressEquals('admin/structure/media');
    $assert_session->pageTextContains('The media type ' . $new_name . ' has been deleted.');

    // Test bundle delete prevention when there is existing media.
    $bundle2 = $this->drupalCreateMediaType(['auto_create_source_field' => TRUE]);
    $label2 = $bundle2->label();
    $media = Media::create(['name' => 'lorem ipsum', 'bundle' => $bundle2->id()]);
    $media->save();
    $this->drupalGet('admin/structure/media/manage/' . $bundle2->id());
    $page->clickLink('Delete');
    $assert_session->addressEquals('admin/structure/media/manage/' . $bundle2->id() . '/delete');
    $assert_session->fieldNotExists('edit-submit');
    $assert_session->pageTextContains("$label2 is used by 1 piece of content on your site. You can not remove this content type until you have removed all of the $label2 content.");
  }

}
