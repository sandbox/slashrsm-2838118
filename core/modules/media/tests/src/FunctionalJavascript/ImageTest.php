<?php

namespace Drupal\Tests\media\FunctionalJavascript;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\media\Entity\Media;

/**
 * Tests for Image Media type.
 *
 * @package Drupal\Tests\media\FunctionalJavascript
 *
 * @group media
 */
class ImageTest extends MediaHandlerTestBase {

  /**
   * Create storage and field instance, attached to a given media type.
   *
   * @param string $field_name
   *   The field name.
   * @param string $field_type
   *   The field storage type.
   * @param string $type_id
   *   The media type machine name.
   */
  protected function createMediaField($field_name, $field_type, $type_id) {
    $storage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'media',
      'type' => $field_type,
    ]);
    $storage->save();

    $config = FieldConfig::create([
      'field_storage' => $storage,
      'bundle' => $type_id,
    ]);
    $config->save();

    // Make the field visible in the form display.
    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $form_display */
    $component = \Drupal::service('plugin.manager.field.widget')
      ->prepareConfiguration($field_type, []);

    entity_get_form_display('media', $type_id, 'default')
      ->setComponent($field_name, $component)
      ->save();
  }

  /**
   * Helper to create a set of fields in a media type.
   *
   * @param array $fields
   *   An associative array where keys are field names and values field types.
   * @param string $type_id
   *   The type machine name.
   */
  protected function createMediaFields(array $fields, $type_id) {
    foreach ($fields as $field_name => $field_type) {
      $this->createMediaField($field_name, $field_type, $type_id);
    }
  }

  /**
   * Just dummy test to check generic methods.
   */
  public function testMediaImagePlugin() {
    $type_name = 'test_media_image_type';
    $source_field_id = 'field_media_image';
    $provided_fields = [
      'mime',
      'width',
      'height',
      'size',
      'created',
      'model',
      'iso',
      'exposure',
      'aperture',
      'focal_length',
    ];

    $session = $this->getSession();
    $page = $session->getPage();
    $assert_session = $this->assertSession();

    // Create image media handler.
    $this->createMediaTypeTest($type_name, 'image', $provided_fields);

    // Create a supported and a non-supported field.
    $fields = [
      'field_string_mime' => 'string',
      'field_string_width' => 'string',
      'field_string_model' => 'string',
    ];
    $this->createMediaFields($fields, $type_name);

    // Hide the media name to test default name generation.
    $this->hideMediaField('name', $type_name);

    $this->drupalGet("admin/structure/media/manage/$type_name");
    $page->selectFieldOption("field_map[mime]", 'field_string_mime');
    $page->selectFieldOption("field_map[width]", 'field_string_width');
    $page->selectFieldOption("field_map[model]", 'field_string_model');
    $page->pressButton('Save');

    // Create a media item.
    $this->drupalGet("media/add/{$type_name}");
    $page->attachFileToField("files[{$source_field_id}_0]", \Drupal::root() . '/core/modules/media/tests/fixtures/exif_example.jpeg');
    $assert_session->assertWaitOnAjaxRequest();
    $page->fillField("{$source_field_id}[0][alt]", 'EXIF Image Alt Text');
    $page->pressButton('Save and publish');

    $assert_session->addressEquals('media/1');

    // Make sure the thumbnail is created from uploaded image.
    $assert_session->elementAttributeContains('css', '.image-style-thumbnail', 'src', 'exif_example.jpeg');

    // Load the media and check that all fields are properly populated.
    $media = Media::load(1);
    $this->assertEquals('exif_example.jpeg', $media->label());
    $this->assertEquals('Drupal EXIF Camera', $media->get('field_string_model')->value);
    $this->assertEquals('200', $media->get('field_string_width')->value);
    $this->assertEquals('image/jpeg', $media->get('field_string_mime')->value);
  }

}
