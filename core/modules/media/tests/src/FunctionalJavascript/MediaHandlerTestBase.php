<?php

namespace Drupal\Tests\media\FunctionalJavascript;

use Drupal\media\Entity\MediaType;

/**
 * A base test class for plugin types.
 */
abstract class MediaHandlerTestBase extends MediaJavascriptTestBase {

  /**
   * Hide a component from the default form display config.
   *
   * @param string $field_name
   *   The field name.
   * @param string $type_name
   *   The media type machine name.
   */
  protected function hideMediaField($field_name, $type_name) {
    $form_display = entity_get_form_display('media', $type_name, 'default');
    $form_display->removeComponent($field_name)->save();
  }

  /**
   * Helper to test a generic type (bundle) creation.
   *
   * @param string $type_name
   *   The type machine name.
   * @param string $type_id
   *   The bundle type ID.
   * @param array $provided_fields
   *   (optional) An array of field machine names this type provides.
   *
   * @return \Drupal\media\MediaTypeInterface
   *   The type created.
   */
  public function createMediaTypeTest($type_name, $type_id, array $provided_fields = []) {
    $session = $this->getSession();
    $page = $session->getPage();
    $assert_session = $this->assertSession();

    $this->drupalGet('admin/structure/media/add');
    $page->fillField('label', $type_name);
    $this->assertJsCondition("jQuery('.machine-name-value').text() === '$type_name'");

    // Make sure the type is available.
    $assert_session->optionExists('handler', $type_id);
    $page->selectFieldOption('handler', $type_id);
    $assert_session->assertWaitOnAjaxRequest();

    // Make sure the provided fields are visible on the form.
    if (!empty($provided_fields)) {
      foreach ($provided_fields as $provided_field) {
        $assert_session->selectExists("field_map[$provided_field]");
      }
    }

    // Save the page to create the type.
    $page->pressButton('Save');
    $assert_session->pageTextContains('The media type ' . $type_name . ' has been added.');
    $this->drupalGet('admin/structure/media');
    $assert_session->pageTextContains($type_name);

    // Bundle definitions are statically cached in the context of the test, we
    // need to make sure we have updated information before proceeding with the
    // actions on the UI.
    \Drupal::service('entity_type.bundle.info')->clearCachedBundles();

    return MediaType::load($type_name);
  }

}
