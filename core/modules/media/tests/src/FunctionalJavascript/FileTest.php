<?php

namespace Drupal\Tests\media\FunctionalJavascript;

use Drupal\media\Entity\Media;

/**
 * Tests the file handler plugin.
 *
 * @group media
 */
class FileTest extends MediaHandlerTestBase {

  /**
   * Tests the file handler.
   */
  public function testFileHandler() {
    $session = $this->getSession();
    $page = $session->getPage();
    $assert_session = $this->assertSession();

    // We rely on an automatic source field being created at this point.
    // @see MediaTypeBase::getSourceFieldName().
    $source_field_name = 'field_media_file';
    $bundle_name = strtolower($this->randomMachineName(12));
    $provided_fields = ['mime', 'size'];
    $this->createMediaTypeTest($bundle_name, 'file', $provided_fields);
    \Drupal::service('entity_field.manager')->clearCachedFieldDefinitions();
    // Adjust the allowed extensions on the source field.
    $this->drupalGet("admin/structure/media/manage/$bundle_name/fields/media.$bundle_name.$source_field_name");
    $page->fillField('settings[file_extensions]', 'txt');
    $page->pressButton('Save settings');
    // Hide the media name to test default name generation.
    $this->hideMediaField('name', $bundle_name);
    // Create a media item.
    $this->drupalGet("media/add/$bundle_name");
    $page->attachFileToField('files[' . $source_field_name . '_0]', \Drupal::root() . '/sites/README.txt');
    $assert_session->assertWaitOnAjaxRequest();
    $page->pressButton('Save and publish');

    $assert_session->addressEquals('media/1');
    // Make sure the thumbnail shows up.
    $assert_session->elementAttributeContains('css', '.image-style-thumbnail', 'src', 'generic.png');
    // Load the media and check its default name.
    $media = Media::load(1);
    $this->assertEquals('README.txt', $media->label());

  }

}
