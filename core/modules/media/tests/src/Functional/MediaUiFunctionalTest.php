<?php

namespace Drupal\Tests\media\Functional;

use Drupal\media\Entity\Media;

/**
 * Ensures that media UI works correctly.
 *
 * @group media
 */
class MediaUiFunctionalTest extends MediaFunctionalTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'block',
    'media_test_handler',
  ];

  /**
   * The test media bundle.
   *
   * @var \Drupal\media\MediaTypeInterface
   */
  protected $testBundle;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('local_tasks_block');
  }

  /**
   * Tests the media actions (add/edit/delete).
   */
  public function testMediaWithOnlyOneBundle() {
    $session = $this->getSession();
    $page = $session->getPage();
    $assert_session = $this->assertSession();

    $bundle = $this->drupalCreateMediaType(['new_revision' => FALSE]);

    $this->drupalGet('media/add');
    $assert_session->statusCodeEquals(200);
    $assert_session->addressEquals('media/add/' . $bundle->id());
    $assert_session->elementNotExists('css', '#edit-revision');

    // Tests media item add form.
    $media_name = $this->randomMachineName();
    $page->fillField('name[0][value]', $media_name);
    $revision_log_message = $this->randomString();
    $page->fillField('revision_log[0][value]', $revision_log_message);
    $page->pressButton('Save and publish');
    $media_id = $this->container->get('entity.query')->get('media')->execute();
    $media_id = reset($media_id);
    /** @var \Drupal\media\MediaInterface $media */
    $media = $this->container->get('entity_type.manager')
      ->getStorage('media')
      ->loadUnchanged($media_id);
    $this->assertEquals($media->getRevisionLogMessage(), $revision_log_message);
    $assert_session->titleEquals($media->label() . ' | Drupal');

    // Tests media edit form.
    $bundle->setNewRevision(FALSE);
    $bundle->save();
    $media_name2 = $this->randomMachineName();
    $this->drupalGet('media/' . $media_id . '/edit');
    $assert_session->checkboxNotChecked('edit-revision');
    $media_name = $this->randomMachineName();
    $page->fillField('name[0][value]', $media_name2);
    $page->pressButton('Save and keep published');
    $assert_session->titleEquals($media_name2 . ' | Drupal');

    // Test that there is no empty vertical tabs element, if the container is
    // empty (see #2750697).
    // Make the "Publisher ID" and "Created" fields hidden.
    $this->drupalGet('/admin/structure/media/manage/' . $bundle->id() . '/form-display');
    $page->selectFieldOption('fields[created][parent]', 'hidden');
    $page->selectFieldOption('fields[uid][parent]', 'hidden');
    $page->pressButton('Save');
    // Assure we are testing with a user without permission to manage revisions.
    $this->drupalLogin($this->nonAdminUser);
    // Check the container is not present.
    $this->drupalGet('media/' . $media_id . '/edit');
    // An empty tab container would look like this.
    $raw_html = '<div data-drupal-selector="edit-advanced" data-vertical-tabs-panes><input class="vertical-tabs__active-tab" data-drupal-selector="edit-advanced-active-tab" type="hidden" name="advanced__active_tab" value="" />' . "\n" . '</div>';
    $assert_session->responseNotContains($raw_html);
    // Continue testing as admin.
    $this->drupalLogin($this->adminUser);

    // Enable revisions by default.
    $bundle->setNewRevision(TRUE);
    $bundle->save();
    $this->drupalGet('media/' . $media_id . '/edit');
    $assert_session->checkboxChecked('edit-revision');
    $page->fillField('name[0][value]', $media_name);
    $page->fillField('revision_log[0][value]', $revision_log_message);
    $page->pressButton('Save and keep published');
    $assert_session->titleEquals($media_name . ' | Drupal');
    /** @var \Drupal\media\MediaInterface $media */
    $media = $this->container->get('entity_type.manager')
      ->getStorage('media')
      ->loadUnchanged($media_id);
    $this->assertEquals($media->getRevisionLogMessage(), $revision_log_message);

    // Tests media delete form.
    $this->drupalGet('media/' . $media_id . '/edit');
    $page->clickLink('Delete');
    $assert_session->pageTextContains('This action cannot be undone');
    $page->pressButton('Delete');
    $media_id = \Drupal::entityQuery('media')->execute();
    $this->assertFalse($media_id);
  }

  /**
   * Tests the "media/add" and "media/mid" pages.
   *
   * Tests if the "media/add" page gives you a selecting option if there are
   * multiple media bundles available.
   */
  public function testMediaWithMultipleBundles() {
    $assert_session = $this->assertSession();

    // Tests and creates the first media bundle.
    $first_media_bundle = $this->drupalCreateMediaType(['description' => $this->randomMachineName(32)]);

    // Test and create a second media bundle.
    $second_media_bundle = $this->drupalCreateMediaType(['description' => $this->randomMachineName(32)]);

    // Test if media/add displays two media bundle options.
    $this->drupalGet('media/add');

    // Checks for the first media bundle.
    $assert_session->pageTextContains($first_media_bundle->label());
    $assert_session->pageTextContains($first_media_bundle->getDescription());
    // Checks for the second media bundle.
    $assert_session->pageTextContains($second_media_bundle->label());
    $assert_session->pageTextContains($second_media_bundle->getDescription());

    // Continue testing media bundle filter.
    $first_media_item = Media::create(['bundle' => $first_media_bundle->id()]);
    $first_media_item->save();
    $second_media_item = Media::create(['bundle' => $second_media_bundle->id()]);
    $second_media_item->save();

    // Go to first media item.
    $this->drupalGet('media/' . $first_media_item->id());
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains($first_media_item->label());

    // Go to second media item.
    $this->drupalGet('media/' . $second_media_item->id());
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains($second_media_item->label());
  }

}
