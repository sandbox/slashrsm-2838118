<?php

namespace Drupal\Tests\media\Functional;

use Drupal\media\Entity\MediaType;

/**
 * Trait with helpers for Media functional tests.
 */
trait MediaFunctionalTestTrait {

  /**
   * Creates a media type.
   *
   * @param array $values
   *   The media type values.
   * @param string $handler
   *   (optional) The handler plugin that is responsible for additional logic
   *   related to this media type.
   *
   * @return \Drupal\media\MediaTypeInterface
   *   A newly created media type.
   */
  protected function createMediaType(array $values = [], $handler = 'test') {
    if (!isset($values['bundle'])) {
      $id = strtolower($this->randomMachineName());
    }
    else {
      $id = $values['bundle'];
    }
    $values += [
      'id' => $id,
      'label' => $id,
      'handler' => $handler,
      'handler_configuration' => [],
      'field_map' => [],
      'new_revision' => FALSE,
    ];

    $bundle = MediaType::create($values);
    $status = $bundle->save();

    $this->assertEquals($status, SAVED_NEW, 'Media type was created successfully.');

    return $bundle;
  }

}
