<?php

namespace Drupal\Tests\media\Kernel;

use Drupal\Core\Language\Language;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\media\Entity\Media;
use Drupal\media\Entity\MediaType;

/**
 * Tests token handling.
 *
 * @group media
 */
class TokensTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'media',
    'media_test_handler',
    'path',
    'file',
    'image',
    'datetime',
    'language',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installEntitySchema('file');
    $this->installSchema('file', 'file_usage');
    $this->installEntitySchema('media');
    $this->installConfig(['language', 'datetime', 'field', 'system']);
  }

  /**
   * Tests some of the tokens provided by Media.
   */
  public function testMediaEntityTokens() {
    // Create a test media bundle.
    $bundle_name = $this->randomMachineName();

    MediaType::create([
      'id' => $bundle_name,
      'label' => $bundle_name,
      'handler' => 'test',
      'handler_configuration' => ['test_config_value' => 'Kakec'],
      'field_map' => [],
      'status' => 1,
      'new_revision' => FALSE,
    ])->save();

    // Create a media entity.
    $media = Media::create([
      'name' => $this->randomMachineName(),
      'bundle' => $bundle_name,
      'uid' => '1',
      'langcode' => Language::LANGCODE_DEFAULT,
      'status' => TRUE,
    ]);
    $media->save();

    $token_service = $this->container->get('token');

    $replaced_value = $token_service->replace('[media:name]', ['media' => $media]);
    $this->assertEquals($media->label(), $replaced_value, 'Token replacement for the media label was successful.');
  }

}
