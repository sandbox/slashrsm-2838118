<?php

namespace Drupal\Tests\media\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\media\Entity\Media;
use Drupal\media\Entity\MediaType;
use Drupal\media\MediaInterface;
use Drupal\media\MediaTypeInterface;

/**
 * Tests creation of Media Bundles and Media Entities.
 *
 * @group media
 */
class BasicCreationTest extends KernelTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = [
    'media',
    'media_test_handler',
    'image',
    'user',
    'field',
    'system',
    'file',
  ];

  /**
   * The test media type.
   *
   * @var \Drupal\media\MediaTypeInterface
   */
  protected $testBundle;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('file');
    $this->installSchema('file', 'file_usage');
    $this->installEntitySchema('media');
    $this->installConfig(['field', 'system', 'image', 'file']);

    // Create a test bundle.
    $id = strtolower($this->randomMachineName());
    $this->testBundle = MediaType::create([
      'id' => $id,
      'label' => $id,
      'handler' => 'test',
      'new_revision' => FALSE,
      'auto_create_source_field' => TRUE,
    ]);
    $this->testBundle->save();
  }

  /**
   * Tests creating a media bundle programmatically.
   */
  public function testMediaBundleCreation() {
    $bundle_storage = $this->container->get('entity_type.manager')->getStorage('media_type');

    $this->assertInstanceOf(MediaTypeInterface::class, MediaType::load($this->testBundle->id()), 'The new media type has not been correctly created in the database.');

    // Test a media type created from default configuration.
    $this->container->get('module_installer')->install(['media_test_type']);
    $test_bundle = $bundle_storage->load('test');
    $this->assertInstanceOf(MediaTypeInterface::class, $test_bundle, 'The media type from default configuration has not been created in the database.');
    $this->assertEquals('Test type', $test_bundle->get('label'), 'Could not assure the correct type name.');
    $this->assertEquals('Test type.', $test_bundle->get('description'), 'Could not assure the correct type description.');
    $this->assertEquals('test', $test_bundle->get('handler'), 'Could not assure the correct handler.');
    // Source field is not set on the handler, but it should never be created
    // automatically when a config is being imported.
    $this->assertEquals(['source_field' => '', 'test_config_value' => 'Kakec'], $test_bundle->get('handler_configuration'), 'Could not assure the correct handler configuration.');
    $this->assertEquals([], $test_bundle->get('field_map'), 'Could not assure the correct field map.');
  }

  /**
   * Tests creating a media entity programmatically.
   */
  public function testMediaEntityCreation() {
    $media = Media::create([
      'bundle' => $this->testBundle->id(),
      'name' => 'Unnamed',
      'field_media_test' => 'Nation of sheep, ruled by wolves, owned by pigs.',
    ]);
    $media->save();

    $this->assertNotInstanceOf(MediaInterface::class, Media::load(rand(1000, 9999)), 'Failed asserting a non-existent media.');

    $this->assertInstanceOf(MediaInterface::class, Media::load($media->id()), 'The new media entity has not been created in the database.');
    $this->assertEquals($this->testBundle->id(), $media->bundle(), 'The media was not created with the correct type.');
    $this->assertEquals('Unnamed', $media->label(), 'The media was not created with the correct name.');
    $this->assertEquals('Nation of sheep, ruled by wolves, owned by pigs.', $media->bundle->entity->getHandler()->getSourceValue($media)->getValue(), 'Handler returns correct source value.');

    // Test the creation of a media without user-defined label and check if a
    // default name is provided.
    $media = Media::create([
      'bundle' => $this->testBundle->id(),
    ]);
    $media->save();
    $expected_name = 'media' . ':' . $this->testBundle->id() . ':' . $media->uuid();
    $this->assertEquals($this->testBundle->id(), $media->bundle(), 'The media was not created with correct type.');
    $this->assertEquals($expected_name, $media->label(), 'The media was not created with a default name.');
  }

  /**
   * Tests creating and updating bundles programmatically.
   */
  public function testProgrammaticBundleManipulation() {
    // Creating a bundle programmatically without specifying a source field
    // should create one automagically.
    /** @var FieldConfig $field */
    $field = $this->testBundle->getHandler()->getSourceField($this->testBundle);
    $this->assertInstanceOf(FieldConfig::class, $field);
    $this->assertEquals('field_media_test', $field->getName());
    $this->assertFalse($field->isNew());

    // Saving with a non-existent source field should create it if the related
    // flag is set.
    $this->testBundle->getHandler()->setConfiguration([
      'source_field' => 'field_magick',
    ]);
    $this->testBundle->set('auto_create_source_field', TRUE);
    $this->testBundle->save();
    $field = $this->testBundle->getHandler()->getSourceField($this->testBundle);
    $this->assertInstanceOf(FieldConfig::class, $field);
    $this->assertEquals('field_magick', $field->getName());
    $this->assertFalse($field->isNew());

    // Trying to save without a source field should create a new, de-duped one.
    $this->testBundle->getHandler()->setConfiguration([]);
    $this->testBundle->set('auto_create_source_field', TRUE);
    $this->testBundle->save();
    $field = $this->testBundle->getHandler()->getSourceField($this->testBundle);
    $this->assertInstanceOf(FieldConfig::class, $field);
    $this->assertEquals('field_media_test_1', $field->getName());
    $this->assertFalse($field->isNew());

    // Trying to reuse an existing field should, well, reuse the existing field
    // even if the auto create flag is set.
    $this->testBundle->getHandler()->setConfiguration([
      'source_field' => 'field_magick',
    ]);
    $this->testBundle->set('auto_create_source_field', TRUE);
    $this->testBundle->save();
    $field = $this->testBundle->getHandler()->getSourceField($this->testBundle);
    $this->assertInstanceOf(FieldConfig::class, $field);
    $this->assertEquals('field_magick', $field->getName());
    $this->assertFalse($field->isNew());
    // No new de-duped fields should have been created.
    $duplicates = FieldConfig::loadMultiple([
      'media.' . $this->testBundle->id() . '.field_magick_1',
      'media.' . $this->testBundle->id() . '.field_media_generic_2',
    ]);
    $this->assertEmpty($duplicates);

    // If auto-create flag is not set fields shouldn't be created.
    // ... if an imaginary field is set.
    $this->testBundle->getHandler()->setConfiguration([
      'source_field' => 'field_imaginary',
    ]);
    $this->testBundle->set('auto_create_source_field', FALSE);
    $this->testBundle->save();
    $field = $this->container->get('entity_type.manager')
      ->getStorage('field_config')
      ->load('media.' . $this->testBundle->id() . '.field_imaginary');
    $field_storage = $this->container->get('entity_type.manager')
      ->getStorage('field_storage_config')
      ->load('media.field_imaginary');
    $this->assertNull($field, 'Field was not automatically created.');
    $this->assertNull($field_storage, 'Field was not automatically created.');

    // ... if no field is set.
    $this->testBundle->getHandler()->setConfiguration([]);
    $this->testBundle->set('auto_create_source_field', FALSE);
    $this->testBundle->save();
    $config = $this->testBundle->getHandler()->getConfiguration();
    $this->assertEquals('', $config['source_field'], 'Source field value was not automatically populated.');
    // Check what would the field be named like if it was created.
    $proposed_field = $this->testBundle->getHandler()->getSourceField($this->testBundle);
    $this->assertInstanceOf(FieldConfig::class, $proposed_field);
    $this->assertEquals('field_media_test_2', $proposed_field->getName());
    $this->assertTrue($proposed_field->isNew());
    $field = $this->container->get('entity_type.manager')
      ->getStorage('field_config')
      ->load('media.' . $this->testBundle->id() . '.' . $proposed_field->getName());
    $field_storage = $this->container->get('entity_type.manager')
      ->getStorage('field_storage_config')
      ->load('media.' . $proposed_field->getName());
    $this->assertNull($field, 'Field was not automatically created.');
    $this->assertNull($field_storage, 'Field was not automatically created.');
  }

}
