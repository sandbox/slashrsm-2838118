(function (Drupal) {

  "use strict";

  Drupal.AjaxCommands.prototype.open_url = function (ajax, response, status) {
    Drupal.ajax({
      dialog: { width: '70%', height: 500 },
      dialogType: 'modal',
      selector: 'body',
      url: response.url
    }).execute();
  };

})(Drupal);
