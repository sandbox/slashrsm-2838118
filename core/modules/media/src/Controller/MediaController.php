<?php

namespace Drupal\media\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Entity\Controller\EntityController;
use Drupal\Core\EventSubscriber\MainContentViewSubscriber;
use Drupal\Core\Url;
use Drupal\file\FileInterface;
use Drupal\media\Ajax\OpenUrlCommand;
use Drupal\media\MediaTypeInterface;
use Drupal\media\SourceFieldInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MediaController extends EntityController {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  public function addPage($entity_type_id, Request $request = NULL) {
    $this->request = $request;
    $build = parent::addPage($entity_type_id);

    if ($build instanceof Response) {
      return $build;
    }

    $bundles = &$build['#bundles'];

    $field_id = $this->request->get('field_id');
    if ($field_id) {
      $handler_settings = $this->entityTypeManager
        ->getStorage('field_config')
        ->load($field_id)
        ->getSetting('handler_settings');

      // Only allow bundles that can be referenced by the field.
      $bundles = array_intersect_key($bundles, array_flip($handler_settings['target_bundles']));
    }

    $fids = $this->request->get('fids');
    if ($fids) {
      /** @var \Drupal\file\FileInterface $file */
      $file = $this->entityTypeManager
        ->getStorage('file')
        ->load($fids[0]);

      // Only allow bundles whose source field supports the file.
      $bundles = array_intersect_key($bundles, $this->getSupportedTypesForFile($file, array_keys($bundles)));
    }

    if (count($bundles) === 1) {
      $route_parameters = [
        'media_type' => key($bundles),
      ];
      return $this->redirect('entity.media.add_form', $route_parameters, [], 302);
    }
    else {
      $bundles = $this->propagate($bundles);
      return $build;
    }
  }

  protected function propagate(array $bundles) {
    foreach ($bundles as $id => $element) {
      $url_options = [
        'query' => $this->request->query->all(),
        'attributes' => [
          'class' => ['use-ajax'],
          'data-dialog-type' => 'modal',
        ],
      ];
      $route_parameters = ['media_type' => $id];
      $url = Url::fromRoute('entity.media.add_form', $route_parameters, $url_options);
      $bundles[$id]['add_link']->setUrl($url);
    }
    return $bundles;
  }

  /**
   * {@inheritdoc}
   */
  protected function redirect($route_name, array $route_parameters = [], array $options = [], $status = 302) {
    $options = array_merge_recursive($options, [
      'query' => $this->request->query->all(),
    ]);
    $redirect = parent::redirect($route_name, $route_parameters, $options, $status);

    if ($this->isAjaxRequest()) {
      return (new AjaxResponse)
        ->addCommand(new OpenUrlCommand($redirect->getTargetUrl()));
    }
    else {
      return $redirect;
    }
  }

  protected function isAjaxRequest() {
    return in_array(
      $this->request->get(MainContentViewSubscriber::WRAPPER_FORMAT),
      ['drupal_modal', 'drupal_ajax']
    );
  }

  protected function getSupportedTypesForFile(FileInterface $file, array $types = NULL) {
    /** @var \Drupal\media\MediaTypeInterface[] $types */
    $types = $this->entityTypeManager
      ->getStorage('media_type')
      ->loadMultiple($types);

    $extension = pathinfo($file->getFilename(), PATHINFO_EXTENSION);

    return array_filter($types, function (MediaTypeInterface $type) use ($extension) {
      $handler = $type->getHandler();
      if ($handler instanceof SourceFieldInterface) {
        $field = $handler->getSourceField($type);
        if (in_array($field->getType(), ['file', 'image'])) {
          return in_array($extension, explode(' ', $field->getSetting('file_extensions')));
        }
      }
      return FALSE;
    });
  }

}
