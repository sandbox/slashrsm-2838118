<?php

namespace Drupal\media;

use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a media entity.
 */
interface MediaInterface extends ContentEntityInterface, EntityChangedInterface, RevisionLogInterface, EntityOwnerInterface, EntityPublishedInterface {

  /**
   * Returns the media creation timestamp.
   *
   * @return int
   *   Creation timestamp of the media.
   */
  public function getCreatedTime();

  /**
   * Sets the media creation timestamp.
   *
   * @param int $timestamp
   *   The media creation timestamp.
   *
   * @return \Drupal\media\MediaInterface
   *   The called media entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the media handler plugin.
   *
   * @return \Drupal\media\MediaHandlerInterface
   *   The handler plugin.
   */
  public function getHandler();

}
