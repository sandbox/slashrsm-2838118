<?php

namespace Drupal\media;

use Drupal\Component\Plugin\ConfigurablePluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Defines the interface for media handler plugins.
 *
 * @see \Drupal\media\Annotation\MediaHandler
 * @see \Drupal\media\MediaHandlerBase
 * @see \Drupal\media\MediaHandlerManager
 * @see plugin_api
 */
interface MediaHandlerInterface extends PluginInspectionInterface, ConfigurablePluginInterface, PluginFormInterface {

  /**
   * Returns the display label of the media handler plugin.
   *
   * @return string
   *   The display label of the media handler plugin.
   */
  public function getLabel();

  /**
   * Gets list of fields provided by this plugin.
   *
   * @return array
   *   Associative array with field names as keys and arrays as values. Each
   *   of those arrays should have the following values:
   *   - label: human-readable label of the field
   *   - field_type: (optional) entity field type that the field should be
   *     mapped to by default. "string" will be assumed if omitted.
   */
  public function getProvidedFields();

  /**
   * Gets a media-related field/value.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media entity.
   * @param string $name
   *   Name of field to fetch.
   *
   * @return mixed|false
   *   Field value or FALSE if data unavailable.
   */
  public function getField(MediaInterface $media, $name);

  /**
   * Attaches handler-specific constraints to media.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media entity.
   */
  public function attachConstraints(MediaInterface $media);

  /**
   * Gets thumbnail image.
   *
   * Media handler plugin is responsible for returning URI of the generic
   * thumbnail if no other is available. This function should always return a
   * valid URI.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media entity.
   *
   * @return string
   *   URI of the thumbnail.
   */
  public function getThumbnail(MediaInterface $media);

  /**
   * Gets the default thumbnail image.
   *
   * @return string
   *   URI of the default thumbnail image.
   */
  public function getDefaultThumbnail();

  /**
   * Provide a default name for the media.
   *
   * Plugins defining media bundles are suggested to override this method and
   * provide a default name, to be used when there is no user-defined label
   * available.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media entity.
   *
   * @return string
   *   The string that should be used as default media name.
   */
  public function getDefaultName(MediaInterface $media);

  /**
   * Maps metadata field to the entity field.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media entity.
   * @param string $source_field
   *   Name of the source metadata field.
   * @param string $destination_field
   *   Name of the destination entity field.
   */
  public function mapFieldValue(MediaInterface $media, $source_field, $destination_field);

}
