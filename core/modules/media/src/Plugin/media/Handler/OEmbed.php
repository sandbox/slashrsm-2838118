<?php

namespace Drupal\media\Plugin\media\Handler;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\link\LinkItemInterface;
use Drupal\media\MediaHandlerBase;
use Drupal\media\MediaInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\media\OEmbedInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the media handler plugin for oEmbed resources.
 *
 * @MediaHandler(
 *   id = "oembed",
 *   label = @Translation("OEmbed"),
 *   description = @Translation("Provides business logic and metadata for oEmbed URLs represented as media."),
 *   allowed_field_types = {"link", "string", "string_long"}
 * )
 */
class OEmbed extends MediaHandlerBase {

  /**
   * The logger channel for media.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * oEmbed service.
   *
   * @var \Drupal\media\OEmbedInterface
   */
  protected $oEmbed;

  /**
   * Constructs a new OEmbed instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel for media.
   * @param \Drupal\media\OEmbedInterface $oembed
   *   The oEmbed service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, ConfigFactoryInterface $config_factory, FieldTypePluginManagerInterface $field_type_manager, LoggerChannelInterface $logger, OEmbedInterface $oembed) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $config_factory, $field_type_manager);
    $this->oEmbed = $oembed;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('config.factory'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('logger.factory')->get('media'),
      $container->get('media.oembed')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getProvidedFields() {
    return [
      'type' => ['label' => $this->t('Resource type')],
      'title' => ['label' => $this->t('Resource title')],
      'author_name' => ['label' => $this->t('The name of the author/owner')],
      'author_url' => ['label' => $this->t('The URL of the author/owner')],
      'provider_name' => ['label' => $this->t("The provider's name")],
      'provider_url' => ['label' => $this->t('The URL of the provider')],
      'cache_age' => ['label' => $this->t('Suggested cache lifetime')],
      'thumbnail_url' => ['label' => $this->t('The URL of the thumbnail')],
      'thumbnail_local_uri' => ['label' => $this->t('The URL of the thumbnail')],
      'thumbnail_local' => ['label' => $this->t('The URL of the thumbnail')],
      'thumbnail_width' => ['label' => $this->t('Thumbnail width')],
      'thumbnail_height' => ['label' => $this->t('Thumbnail height')],
      'url' => ['label' => $this->t('The source URL of the resource')],
      'width' => ['label' => $this->t('The width of the resource')],
      'height' => ['label' => $this->t('The height of the resource')],
      'html' => ['label' => $this->t('HTML representation of the resource')],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getField(MediaInterface $media, $name) {
    $main_property = $media->get($this->configuration['source_field'])->first()->mainPropertyName();
    $resource_url = $media->get($this->configuration['source_field'])->first()->get($main_property)->getString();
    $oembed_data = $this->oEmbed->fetchResource($this->oEmbed->doUrlDiscovery($resource_url));

    switch ($name) {
      case 'thumbnail_local':
        $local_uri = $this->getField($media, 'thumbnail_local_uri');

        if ($local_uri) {
          if (file_exists($local_uri)) {
            return $local_uri;
          }
          else {
            $image_data = file_get_contents($this->getField($media, 'thumbnail_url'));
            if ($image_data) {
              return file_unmanaged_save_data($image_data, $local_uri, FILE_EXISTS_REPLACE);
            }
          }
        }
        return FALSE;

      case 'thumbnail_local_uri':
        $image_url = $this->getField($media, 'thumbnail_url');
        if ($image_url) {
          $filename = $this->getField($media, 'provider_name') . '_' . substr(md5($resource_url), 0, 5);
          return $this->getLocalImageUri($filename, $image_url);
        }
        return FALSE;

      default:
        if (!empty($oembed_data[$name])) {
          return $oembed_data[$name];
        }
        break;

    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['thumbnails_location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Thumbnails location'),
      '#default_value' => $this->configuration['thumbnails_location'],
      '#description' => $this->t('Thumbnails will be fetched from the provider for local usage. This is the location where they will be placed.'),
    ];

    /*$form['allowed_providers'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#default_value' => $this->configuration['allowed_providers'],
      '#options' => array_map(function ($provider) { return $provider['provider_name']; }, $this->oEmbed->getProviders()),
      '#description' => $this->t('Select allowed oEmbed providers for this media type.'),
      '#attributes' => ['size' => 30],
    ];*/

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'thumbnails_location' => 'public://oembed_thumbnails',
      //'allowed_providers' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultThumbnail() {
    // TODO Use oEmbed specific thumbnail?
    return $this->configFactory->get('media.settings')
      ->get('icon_base') . '/no-thumbnail.png';
  }

  /**
   * {@inheritdoc}
   */
  public function getThumbnail(MediaInterface $media) {
    if ($local_image = $this->getField($media, 'thumbnail_local')) {
      return $local_image;
    }
    return $this->getDefaultThumbnail();
  }

  /**
   * {@inheritdoc}
   */
  protected function createSourceField(MediaTypeInterface $type) {
    $field = parent::createSourceField($type);

    $settings = $field->getSettings();
    $settings['title'] = DRUPAL_DISABLED;
    $settings['link_type'] = LinkItemInterface::LINK_EXTERNAL;
    $field->set('settings', $settings);

    return $field;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultName(MediaInterface $media) {
    if ($title = $this->getField($media, 'title')) {
      return $title;
    }
    return parent::getDefaultName($media);
  }

  /**
   * {@inheritdoc}
   */
  public function attachConstraints(MediaInterface $media) {
    parent::attachConstraints($media);

    if (isset($this->configuration['source_field'])) {
      if ($media->hasField($this->configuration['source_field'])) {
        foreach ($media->get($this->configuration['source_field']) as &$url) {
          /** @var \Drupal\Core\TypedData\DataDefinitionInterface $typed_data */
          $typed_data = $url->getDataDefinition();
          $typed_data->addConstraint('oembed_resource');
          $typed_data->addConstraint('oembed_provider');
        }
      }
    }
  }

  /**
   * Computes the destination URI for a thumbnail
   *
   * @param string
   *   Filename without the extension.
   * @param string $remote_url
   *   The remote URL of the thumbnail.
   *
   * @return string
   *   The local URI.
   */
  protected function getLocalImageUri($filename, $remote_url) {
    // Ensure that the destination directory is writable. If not, log a warning
    // and return the default thumbnail.
    $ready = file_prepare_directory($directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    if (!$ready) {
      $this->logger->warning('Could not prepare thumbnail destination directory @dir for oEmbed media.', [
        '@dir' => $directory,
      ]);
      return $this->getDefaultThumbnail();
    }

    $local_uri = $this->configuration['thumbnails_location'] . '/' . $filename . '.';
    $local_uri .= pathinfo($remote_url, PATHINFO_EXTENSION);

    return $local_uri;
  }

}