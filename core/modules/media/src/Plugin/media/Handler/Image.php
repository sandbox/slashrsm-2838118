<?php

namespace Drupal\media\Plugin\media\Handler;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\media\MediaInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the media handler plugin for Images.
 *
 * @MediaHandler(
 *   id = "image",
 *   label = @Translation("Image"),
 *   description = @Translation("Provides business logic and metadata for local images."),
 *   allowed_field_types = {"image", "file"}
 * )
 */
class Image extends File {

  /**
   * The image factory service.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * File system service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * The EXIF data.
   *
   * @var array
   */
  protected $exif;

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Media entity config object.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   * @param \Drupal\Core\Image\ImageFactory $image_factory
   *   The image factory.
   * @param \Drupal\Core\File\FileSystem $file_system
   *   File system service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, ConfigFactoryInterface $config, FieldTypePluginManagerInterface $field_type_manager, ImageFactory $image_factory, FileSystem $file_system) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $config, $field_type_manager);

    $this->imageFactory = $image_factory;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('config.factory'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('image.factory'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getProvidedFields() {
    $fields = parent::getProvidedFields();

    $fields += [
      'width' => ['label' => $this->t('Width')],
      'height' => ['label' => $this->t('Height')],
    ];

    if ($this->canReadExifData()) {
      $fields += [
        'model' => ['label' => $this->t('Camera model')],
        'created' => ['label' => $this->t('Image creation datetime')],
        'iso' => ['label' => $this->t('Iso')],
        'exposure' => ['label' => $this->t('Exposure time')],
        'aperture' => ['label' => $this->t('Aperture value')],
        'focal_length' => ['label' => $this->t('Focal length')],
      ];
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getField(MediaInterface $media, $name) {
    $value = parent::getField($media, $name);

    if ($value !== FALSE) {
      return $value;
    }

    // Get the file, image and EXIF data.
    $file = $this->getSourceFieldValue($media);
    $uri = $file->getFileUri();
    $image = $this->imageFactory->get($uri);

    // Return the field.
    switch ($name) {
      case 'width':
        return $image->getWidth() ?: FALSE;

      case 'height':
        return $image->getHeight() ?: FALSE;
    }

    if ($this->canReadExifData()) {
      switch ($name) {
        case 'created':
          /** @var \DateTime $date */
          $date = new DrupalDateTime($this->getExifField($uri, 'DateTimeOriginal'));
          return $date->getTimestamp();

        case 'model':
          return $this->getExifField($uri, 'Model');

        case 'iso':
          return $this->getExifField($uri, 'ISOSpeedRatings');

        case 'exposure':
          return $this->getExifField($uri, 'ExposureTime');

        case 'aperture':
          return $this->getExifField($uri, 'FNumber');

        case 'focal_length':
          return $this->getExifField($uri, 'FocalLength');
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Show message if it's not possible to read EXIF data.
    if (!$this->canReadExifData()) {
      $form['no_exif_data_reader'] = [
        '#markup' => $this->t('Unable to read EXIF data for Image. In order to provide EXIF data reading functionality please take a look at PHP documentation of <a href="https://secure.php.net/exif_read_data" target="_blank">exif_read_data</a> function.'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultThumbnail() {
    return $this->configFactory->get('media.settings')
      ->get('icon_base') . '/no-thumbnail.png';
  }

  /**
   * {@inheritdoc}
   */
  public function getThumbnail(MediaInterface $media) {
    return $this->getSourceFieldValue($media)->getFileUri();
  }

  /**
   * Check does functionality for reading EXIF data exist.
   *
   * @return bool
   *   Returns TRUE if functionality for reading of EXIF data is provided.
   */
  protected function canReadExifData() {
    return function_exists('exif_read_data');
  }

  /**
   * Get EXIF field value.
   *
   * @param string $uri
   *   The uri for the file that we are getting the EXIF.
   * @param string $field
   *   The name of the EXIF field.
   *
   * @return string|bool
   *   The value for the requested field or FALSE if is not set.
   */
  protected function getExifField($uri, $field) {
    if (empty($this->exif)) {
      $this->exif = $this->getExifData($uri);
    }

    return $this->exif[$field] ?: FALSE;
  }

  /**
   * Read EXIF.
   *
   * @param string $uri
   *   The uri for the file that we are getting the Exif.
   *
   * @return array|bool
   *   An associative array where the array indexes are the header names and
   *   the array values are the values associated with those headers or FALSE
   *   if the data can't be read.
   */
  protected function getExifData($uri) {
    return exif_read_data($this->fileSystem->realpath($uri), 'EXIF');
  }

}
