<?php

namespace Drupal\media\Plugin\media\Handler;

use Drupal\media\MediaInterface;
use Drupal\media\MediaHandlerBase;

/**
 * Provides the media handler plugin for Files.
 *
 * @MediaHandler(
 *   id = "file",
 *   label = @Translation("File"),
 *   description = @Translation("Provides business logic and metadata for local files and documents."),
 *   allowed_field_types = {"file"},
 * )
 */
class File extends MediaHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function getProvidedFields() {
    return [
      'mime' => ['label' => $this->t('MIME type')],
      'size' => ['label' => $this->t('Size')],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getField(MediaInterface $media, $name) {
    $file = $this->getSourceFieldValue($media);

    switch ($name) {
      case 'mime':
        return $file->getMimeType() ?: FALSE;

      case 'size':
        $size = $file->getSize();
        return is_numeric($size) ? $size : FALSE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getThumbnail(MediaInterface $media) {
    $file = $this->getSourceFieldValue($media);
    $icon_base = $this->configFactory->get('media.settings')->get('icon_base');
    $thumbnail = FALSE;

    // We try to magically use the most specific icon present in the $icon_base
    // directory, based on the mime information. For instance, if an icon file
    // named "pdf.png" is present, it will be used if the file matches this
    // mime type.
    if ($file) {
      $mimetype = $file->getMimeType();
      $mimetype = explode('/', $mimetype);
      $thumbnail = $icon_base . "/{$mimetype[0]}--{$mimetype[1]}.png";

      if (!is_file($thumbnail)) {
        $thumbnail = $icon_base . "/{$mimetype[1]}.png";
      }
    }

    if (!is_file($thumbnail)) {
      $thumbnail = $icon_base . '/generic.png';
    }

    return $thumbnail;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultName(MediaInterface $media) {
    // The default name will be the filename of the source_field.
    return $this->getSourceFieldValue($media)->getFilename();
  }

  /**
   * Get source field file entity.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media entity.
   *
   * @return \Drupal\file\FileInterface
   *   The file entity present in the source field.
   */
  protected function getSourceFieldValue(MediaInterface $media) {
    $source_field = $this->configuration['source_field'];

    if (empty($source_field)) {
      throw new \RuntimeException('Source field for media file handler is not defined.');
    }

    $file = $media->get($source_field)->entity;
    if (empty($file)) {
      throw new \RuntimeException('The source field does not contain a file entity.');
    }

    return $file;
  }

}
