<?php

namespace Drupal\media\Plugin\QueueWorker;

use Drupal\media\Entity\Media;
use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Download images.
 *
 * @QueueWorker(
 *   id = "media_entity_thumbnail",
 *   title = @Translation("Thumbnail downloader"),
 *   cron = {"time" = 60}
 * )
 */
class ThumbnailDownloader extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    /** @var \Drupal\media\MediaInterface $entity */
    if ($entity = Media::load($data['id'])) {
      $entity->automaticallySetThumbnail();
      $entity->save();
    }
  }

}
