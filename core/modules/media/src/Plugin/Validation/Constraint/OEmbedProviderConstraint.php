<?php

namespace Drupal\media\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a value belongs to an allowed oEmbed provider.
 *
 * @Constraint(
 *   id = "oembed_provider",
 *   label = @Translation("oEmbed provider", context = "Validation"),
 *   type = { "link"}
 * )
 */
class OEmbedProviderConstraint extends Constraint {

  /**
   * The default violation message.
   *
   * @var string
   */
  public $message = 'oEmbed provider @provider is not allowed.';

}
