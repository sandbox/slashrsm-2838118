<?php

namespace Drupal\media\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a value belongs to an allowed oEmbed provider.
 *
 * @Constraint(
 *   id = "oembed_resource",
 *   label = @Translation("oEmbed resource", context = "Validation"),
 *   type = { "link", "string", "string_long"}
 * )
 */
class OEmbedResourceConstraint extends Constraint {

  /**
   * The default violation message.
   *
   * @var string
   */
  public $message = 'Provided URL does not represent a valid oEmbed resource.';

}
