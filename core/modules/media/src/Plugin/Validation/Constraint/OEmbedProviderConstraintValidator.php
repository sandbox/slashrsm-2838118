<?php

namespace Drupal\media\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\media\OEmbedInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the OEmbedProvider constraint.
 */
class OEmbedProviderConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The oEmbed service.
   *
   * @var \Drupal\media\OEmbed
   */
  protected $oEmbed;

  /**
   * Constructs a new TweetVisibleConstraintValidator.
   *
   * @param \Drupal\media\OEmbedInterface $oembed
   *   The oEmbed service.
   */
  public function __construct(OEmbedInterface $oembed) {
    $this->oEmbed = $oembed;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('media.oembed'));
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    // TODO check if resource belongs to an allowed provider.


    //$this->context->addViolation($constraint->message, ['@provider' => 'Not allowed provider']);
  }

}
