<?php

namespace Drupal\media\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;
use Drupal\media\Entity\MediaType;

/**
 * Plugin implementation of the 'media_file_default' formatter.
 *
 * @FieldFormatter(
 *   id = "media_file_default",
 *   label = @Translation("Generic media file"),
 *   weight = 2,
 *   field_types = {
 *     "entity_reference"
 *   },
 *   media_handlers = {
 *     "file"
 *   }
 * )
 */
class MediaGenericFileFormatter extends FileFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    if (parent::isApplicable($field_definition) && $field_definition->getSetting('target_type') == 'media') {
      /** @var MediaType[] $bundles */
      $allowed_types = MediaType::loadMultiple($field_definition->getSetting('handler_settings')['target_bundles']);

      $media_handlers = [];
      $definitions = \Drupal::service('plugin.manager.field.formatter')->getDefinitions();
      foreach ($definitions as $definition) {
        if ($definition['class'] == static::class) {
          $media_handlers = $definition['media_handlers'];
        }
      }

      foreach ($allowed_types as $type) {
        /** @var \Drupal\media\Entity\MediaType $type */
        if (in_array($type->getHandler()->getPluginId(), $media_handlers)) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  protected function needsEntityLoad(EntityReferenceItem $item) {
    return TRUE;
  }


  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();
    $referenced_entities = $items->referencedEntities();
    foreach ($items->referencedEntities() as $delta => $entity) {
      $source_field_name = $entity->getHandler()->getConfiguration()['source_field'];
      /** @var \Drupal\file\FileInterface $file */
      $file = $entity->get($source_field_name)->entity;

      $elements[$delta] = array(
        '#theme' => 'file_link',
        '#file' => $file,
        '#description' => $file->description,
        '#cache' => array(
          'tags' => $file->getCacheTags(),
        ),
      );
      // Pass field item attributes to the theme function.
      if (isset($item->_attributes)) {
        $elements[$delta] += array('#attributes' => array());
        $elements[$delta]['#attributes'] += $item->_attributes;
        // Unset field item attributes since they have been included in the
        // formatter output and should not be rendered in the field template.
        unset($item->_attributes);
      }
    }

    return $elements;
  }

}
