<?php

namespace Drupal\media\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;
use Drupal\media\Entity\MediaType;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;

/**
 * Plugin implementation of the 'media_file_url_plain' formatter.
 *
 * @FieldFormatter(
 *   id = "media_file_url_plain",
 *   label = @Translation("URL to media file"),
 *   weight = 1,
 *   field_types = {
 *     "entity_reference"
 *   },
 *   media_handlers = {
 *     "file"
 *   }
 * )
 */
class MediaUrlPlainFormatter extends FileFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    if (parent::isApplicable($field_definition) && $field_definition->getSetting('target_type') == 'media') {
      /** @var MediaType[] $bundles */
      $allowed_types = MediaType::loadMultiple($field_definition->getSetting('handler_settings')['target_bundles']);

      $media_handlers = [];
      $definitions = \Drupal::service('plugin.manager.field.formatter')->getDefinitions();
      foreach ($definitions as $definition) {
        if ($definition['class'] == static::class) {
          $media_handlers = $definition['media_handlers'];
        }
      }

      foreach ($allowed_types as $type) {
        /** @var \Drupal\media\Entity\MediaType $type */
        if (in_array($type->getHandler()->getPluginId(), $media_handlers)) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  protected function needsEntityLoad(EntityReferenceItem $item) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileUri() {
    return $this->get('uri')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();

    foreach($items->referencedEntities() as $entity) {
      $source_field_name = $entity->getHandler()->getConfiguration()['source_field'];
      /** @var \Drupal\file\FileInterface $file */
      $file = $entity->get($source_field_name)->entity;
      $elements[] = array(
        '#markup' => file_url_transform_relative(file_create_url($file->getFileUri())),
        '#cache' => array(
          'tags' => $file->getCacheTags(),
        ),
      );
    }

    return $elements;
  }

}
