<?php

namespace Drupal\media\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'link' formatter.
 *
 * @FieldFormatter(
 *   id = "oembed",
 *   label = @Translation("oEmbed"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class OEmbedFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $resource = \Drupal::service('media.oembed')->fetchResource(\Drupal::service('media.oembed')->doUrlDiscovery($item->uri));
      $element[$delta] = [
        'data' => [
          '#markup' => $resource['html'],
          '#allowed_tags' => ['iframe'],
        ],
      ];
    }

    return $element;
  }

}
