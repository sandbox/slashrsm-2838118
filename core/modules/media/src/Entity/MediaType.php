<?php

namespace Drupal\media\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\Core\Plugin\DefaultSingleLazyPluginCollection;
use Drupal\field\FieldStorageConfigInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\media\MediaInterface;
use Drupal\media\SourceFieldInterface;

/**
 * Defines the Media type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "media_type",
 *   label = @Translation("Media type"),
 *   label_singular = @Translation("media type"),
 *   label_plural = @Translation("media types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count media type",
 *     plural = "@count media types"
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\media\MediaTypeForm",
 *       "edit" = "Drupal\media\MediaTypeForm",
 *       "delete" = "Drupal\media\Form\MediaTypeDeleteConfirmForm"
 *     },
 *     "list_builder" = "Drupal\media\MediaTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer media types",
 *   config_prefix = "type",
 *   bundle_of = "media",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "handler",
 *     "queue_thumbnail_downloads",
 *     "new_revision",
 *     "handler_configuration",
 *     "field_map",
 *     "status",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/media/add",
 *     "edit-form" = "/admin/structure/media/manage/{media_type}",
 *     "delete-form" = "/admin/structure/media/manage/{media_type}/delete",
 *     "collection" = "/admin/structure/media",
 *   },
 * )
 */
class MediaType extends ConfigEntityBundleBase implements MediaTypeInterface, EntityWithPluginCollectionInterface {

  /**
   * The machine name of this media type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the media type.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of this media type.
   *
   * @var string
   */
  protected $description;

  /**
   * The handler plugin ID.
   *
   * @var string
   */
  protected $handler;

  /**
   * Whether media items should be published by default.
   *
   * @var bool
   */
  protected $status = TRUE;

  /**
   * Whether thumbnail downloads are queued.
   *
   * @var bool
   */
  protected $queue_thumbnail_downloads = FALSE;

  /**
   * Default value of the 'Create new revision' checkbox of this media type.
   *
   * @var bool
   */
  protected $new_revision = FALSE;

  /**
   * The handler plugin configuration.
   *
   * @var array
   */
  protected $handler_configuration = [];

  /**
   * Lazy collection for the handler plugin.
   *
   * @var \Drupal\Core\Plugin\DefaultSingleLazyPluginCollection
   */
  protected $handlerPluginCollection;

  /**
   * Field map. Fields provided by type plugin to be stored as entity fields.
   *
   * @var array
   */
  protected $field_map = [];

  /**
   * Whether to auto-create source filed.
   *
   * @var bool
   */
  protected $auto_create_source_field = FALSE;

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return [
      'handler_configuration' => $this->handlerPluginCollection(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel(MediaInterface $media) {
    $bundle = static::load($media->bundle());
    return $bundle ? $bundle->label() : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueueThumbnailDownloadsStatus() {
    return $this->queue_thumbnail_downloads;
  }

  /**
   * {@inheritdoc}
   */
  public function setQueueThumbnailDownloadsStatus($queue_thumbnail_downloads) {
    $this->queue_thumbnail_downloads = $queue_thumbnail_downloads;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHandler() {
    return $this->handlerPluginCollection()->get($this->handler);
  }

  /**
   * Returns handler lazy plugin collection.
   *
   * @return \Drupal\Core\Plugin\DefaultSingleLazyPluginCollection
   *   The tag plugin collection.
   */
  protected function handlerPluginCollection() {
    if (!$this->handlerPluginCollection) {
      $this->handlerPluginCollection = new DefaultSingleLazyPluginCollection(\Drupal::service('plugin.manager.media.handler'), $this->handler, $this->handler_configuration);
    }
    return $this->handlerPluginCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldCreateNewRevision() {
    return $this->new_revision;
  }

  /**
   * {@inheritdoc}
   */
  public function setNewRevision($new_revision) {
    $this->new_revision = $new_revision;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    // If the handler uses a source field, we'll need to store its name before
    // saving. We'd need to double-save if we did this in postSave().
    $handler = $this->getHandler();
    if ($this->auto_create_source_field && $handler instanceof SourceFieldInterface) {
      $field_storage = $handler->getSourceField($this)->getFieldStorageDefinition();
      // If the field storage is a new (unsaved) config entity, save it.
      if ($field_storage instanceof FieldStorageConfigInterface && $field_storage->isNew()) {
        $field_storage->save();
      }
    }

    parent::preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // If the handler is using a source field, we may need to save it if it's
    // new. The field storage is guaranteed to exist already because preSave()
    // took care of that.
    $handler = $this->getHandler();
    if ($this->auto_create_source_field && $handler instanceof SourceFieldInterface) {
      $field = $handler->getSourceField($this);

      // If the field is new, save it and add it to this bundle's view and form
      // displays.
      if ($field->isNew()) {
        // Ensure the field is saved correctly before adding it to the displays.
        $field->save();

        $entity_type = $field->getTargetEntityTypeId();
        $bundle = $field->getTargetBundle();

        if ($field->isDisplayConfigurable('form')) {
          // Use the default widget and settings.
          $component = \Drupal::service('plugin.manager.field.widget')
            ->prepareConfiguration($field->getType(), []);

          entity_get_form_display($entity_type, $bundle, 'default')
            ->setComponent($field->getName(), $component)
            ->save();
        }
        if ($field->isDisplayConfigurable('view')) {
          // Use the default formatter and settings.
          $component = \Drupal::service('plugin.manager.field.formatter')
            ->prepareConfiguration($field->getType(), []);

          entity_get_display($entity_type, $bundle, 'default')
            ->setComponent($field->getName(), $component)
            ->save();
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldMap() {
    return $this->field_map;
  }

}
