<?php

namespace Drupal\media;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Http\ClientFactory;

/**
 * OEmbed service.
 */
class OEmbed implements OEmbedInterface {

  /**
   * Cache key to be used to store providers info into cache.
   *
   * @var string
   */
  protected static $providersCacheKey = 'media:oembed:providers';

  /**
   * URL of the JSON with providers info.
   *
   * @var string
   */
  protected static $providersUrl = 'http://oembed.com/providers.json';

  /**
   * Cache lifetime for the providers info.
   */
  protected static $providersCacheLifetime = 604800;

  /**
   * The HTTP client factory service.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $clientFactory;

  /**
   * OEmbed providers information.
   *
   * @var array
   */
  protected $providers;

  /**
   * Cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Static cache of fetched oEmbed resources.
   *
   * @var array
   */
  protected $resources;

  /**
   * Static cache of discovered oEmbed resources.
   *
   * @var array
   */
  protected $discovered;

  /**
   * Constructs OEmbed class.
   *
   * @param \Drupal\Core\Http\ClientFactory $client_factory
   *   The HTTP client factory service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   */
  public function __construct(ClientFactory $client_factory, CacheBackendInterface $cache) {
    $this->clientFactory = $client_factory;
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public function getProviders() {
    if ($this->providers) {
      return $this->providers;
    }
    elseif ($data = $this->cache->get(static::$providersCacheKey)) {
      $this->providers = $data->data;
      return $this->providers;
    }
    else {
      $response = $this->clientFactory->fromOptions()->get(static::$providersUrl);
      if ($response->getStatusCode() !== 200) {
        // TODO Handle errors.
      }

      $providers = \GuzzleHttp\json_decode($response->getBody()->getContents(), TRUE);

      if (!is_array($providers)) {
        // TODO Handle errors.
      }

      $this->cache->set(self::$providersCacheKey, $providers, static::$providersCacheLifetime);
      $this->providers = $providers;
      return $this->providers;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function matchUrl($url, $providers_limit = NULL) {
    $providers = $this->getProviders();
    if (isset($providers)) {
      $providers = array_intersect_key($providers, $providers_limit);
    }

    foreach ($providers as $provider_name => $provider_info) {
      if (!empty($provider_info['endpoints'])) {
        foreach ($provider_info['endpoints'] as $endpoint) {
          if (!empty($endpoint['schemes'])) {
            foreach ($endpoint['schemes'] as $scheme) {
              $regexp = str_replace(['.', '*'], ['\.', '.*'], $scheme);
              if (preg_match("|$regexp|", $url)) {
                return [
                  'provider' => $provider_name,
                  'endpoint' => $endpoint['url'],
                ];
              }
            }
          }
        }
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function doUrlDiscovery($url) {
    if (!empty($this->discovered[$url])) {
      return $this->discovered[$url];
    }

    $response = $this->clientFactory->fromOptions()->get($url);
    if ($response->getStatusCode() !== 200) {
      // TODO
    }

    $content = $response->getBody()->getContents();
    $dom = new \DOMDocument();

    // TODO this causes annoying warning with YT videos. Fix it:
    // Warning: DOMDocument::loadHTML(): htmlParseEntityRef: no name in Entity
    if (!@$dom->loadHTML($content)) {
      // TODO
    }

    $xpath = new \DOMXpath($dom);
    $result = $xpath->query("//link[@type='application/json+oembed']");
    if ($result->length > 0) {
      $this->discovered[$url] = $result->item(0)->getAttribute('href');
      return $this->discovered[$url];
    }

    $result = $xpath->query("//link[@type='text/xml+oembed']");
    if ($result->length > 0) {
      $this->discovered[$url] = $result->item(0)->getAttribute('href');
      return $this->discovered[$url];
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchResource($endpoint_url) {
    if (!empty($this->resources[$endpoint_url])) {
      return $this->resources[$endpoint_url];
    }

    $response = $this->clientFactory->fromOptions()->get($endpoint_url);
    if ($response->getStatusCode() !== 200) {
      // TODO Handle errors.
    }

    $format = $response->getHeader('Content-Type');
    $content = $response->getBody()->getContents();
    if ($format[0] == 'application/json') {
      $data = \GuzzleHttp\json_decode($content, TRUE);

      if (!is_array($data)) {
        // TODO Handle errors.
      }

      $this->resources[$endpoint_url] = $data;
      return $data;
    }
    elseif ($format[0] == 'text/xml') {
      $data = \GuzzleHttp\json_decode(\GuzzleHttp\json_encode($content), TRUE);

      if (!is_array($data)) {
        // TODO Handle errors.
      }

      $this->resources[$endpoint_url] = $data;
      return $data;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isOEmbedResource($url) {
    if ($this->doUrlDiscovery($url)) {
      return TRUE;
    }

    // TODO handle providers that do not support discovery.

    return FALSE;
  }

}