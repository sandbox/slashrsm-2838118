<?php

namespace Drupal\media;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\media\Annotation\MediaHandler;

/**
 * Manages media handler plugins.
 */
class MediaHandlerManager extends DefaultPluginManager {

  /**
   * Constructs a new MediaHandlerManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/media/Handler', $namespaces, $module_handler, MediaHandlerInterface::class, MediaHandler::class);

    $this->alterInfo('media_handler_info');
    $this->setCacheBackend($cache_backend, 'media_handler_plugins');
  }

}
