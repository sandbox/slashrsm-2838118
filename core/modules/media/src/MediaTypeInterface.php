<?php

namespace Drupal\media;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;
use Drupal\Core\Entity\RevisionableEntityBundleInterface;

/**
 * Provides an interface defining a media bundle entity.
 */
interface MediaTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface, RevisionableEntityBundleInterface {

  /**
   * Returns the label.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media entity.
   *
   * @return string|bool
   *   Returns the label of the bundle that entity belongs to.
   */
  public static function getLabel(MediaInterface $media);

  /**
   * Returns whether thumbnail downloads are queued.
   *
   * @return bool
   *   TRUE if thumbnails are queued for download later, FALSE if they should be
   *   downloaded now.
   */
  public function getQueueThumbnailDownloadsStatus();

  /**
   * Sets a flag to indicate that thumbnails should be downloaded via a queue.
   *
   * @param bool $queue_thumbnail_downloads
   *   The queue downloads flag.
   *
   * @return $this
   */
  public function setQueueThumbnailDownloadsStatus($queue_thumbnail_downloads);

  /**
   * Returns the media handler plugin.
   *
   * @return \Drupal\media\MediaHandlerInterface
   *   The handler plugin.
   */
  public function getHandler();

  /**
   * Sets whether new revisions should be created by default.
   *
   * @param bool $new_revision
   *   TRUE if media items of this type should create new revisions by default.
   *
   * @return $this
   */
  public function setNewRevision($new_revision);

  /**
   * Returns the metadata field map.
   *
   * Field mapping allows site builders to map media item related metadata to
   * entity fields. This information will be used when saving a given media item
   * and if metadata values will be available they are going to be automatically
   * copied to the corresponding entity fields.
   *
   * @return array
   *   Field mapping array with fields provided by the type plugin as keys and
   *   Drupal Entity fields as values.
   */
  public function getFieldMap();

}
