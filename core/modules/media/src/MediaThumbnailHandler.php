<?php

namespace Drupal\media;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Provides a service for handling media thumbnails.
 */
class MediaThumbnailHandler implements MediaThumbnailHandlerInterface {

  use StringTranslationTrait;

  /**
   * The file entity storage handler.
   *
   * @var \Drupal\file\FileStorageInterface
   */
  protected $fileStorage;

  /**
   * Constructs a new MediaThumbnailHandler.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   The string translation service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TranslationInterface $translation) {
    $this->fileStorage = $entity_type_manager->getStorage('file');
    $this->stringTranslation = $translation;
  }

  /**
   * {@inheritdoc}
   */
  public function setThumbnail(MediaInterface $media) {
    // If thumbnail fetching should be queued then temporary use default
    // thumbnail or fetch it immediately otherwise.
    if ($media->bundle->entity->getQueueThumbnailDownloadsStatus() && $media->isNew()) {
      $thumbnail_uri = $media->getHandler()->getDefaultThumbnail();
    }
    else {
      $thumbnail_uri = $media->getHandler()->getThumbnail($media);
    }
    $existing = $this->fileStorage->getQuery()
      ->condition('uri', $thumbnail_uri)
      ->execute();

    if ($existing) {
      $media->thumbnail->target_id = reset($existing);
    }
    else {
      /** @var \Drupal\file\FileInterface $file */
      $file = $this->fileStorage->create(['uri' => $thumbnail_uri]);
      if ($owner = $media->getOwner()) {
        $file->setOwner($owner);
      }
      $file->setPermanent();
      $file->save();
      $media->thumbnail->target_id = $file->id();
    }

    $media->thumbnail->alt = $this->t('Thumbnail');
    $media->thumbnail->title = $media->label();
  }

}
