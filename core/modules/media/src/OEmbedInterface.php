<?php

namespace Drupal\media;

/**
 * Interface for oEmbed service.
 */
interface OEmbedInterface {

  /**
   * Gets oEmbed providers information.
   *
   * Returns multi-dimensional array where each provider is represented as a
   * single top-level array element. Each individual element has the following
   * values:
   * - provider_name: Human readable name of the provider.
   * - provider_url: Main URL of the provider.
   * - endpoints: List of endpoints provider exposes. Each endpoint is an array
   *   with the following values:
   *     - url: Endpoint's URL.
   *     - schemes: List of URL schemes supported by the provider.
   *     - formats: List of supported formats. Can be "json", "xml" or both.
   *     - discovery: Whether provider supports oEmbed discovery.
   *
   * @return array
   *   Information about oEmbed providers.
   */
  public function getProviders();

  /**
   * Tries to match the URL against the database of oEmbed providers.
   *
   * @param string $url
   *   URL to be tested
   * @param array|null $providers_limit
   *   List of provider machine names to limit too. All providers are considered
   *   if omitted.
   *
   * @return array|false
   *   Associative array with two values:
   *   - provider: Matching provider's name.
   *   - endpoint: URL of the matching endpoint.
   *   FALSE if a match was not found.
   */
  public function matchUrl($url, $providers_limit = NULL);

  /**
   * Runs oEmbed discovery and returns endpoint URL if successful.
   *
   * @param $url
   *   URL of the resource.
   *
   * @return string|false
   *   Resource specific URL of the oEmbed endpoint or FALSE if the discovery
   *   was not successful.
   */
  public function doUrlDiscovery($url);


  /**
   * Determines whether it is an oEmbed resource.
   *
   * @param $url
   *   URL of the resource.
   *
   * @return bool
   *   TRUE if the URL represents a valid oEmbed resource and FALSE if not.
   */
  public function isOEmbedResource($url);

  /**
   * Fetches information about the oEmbed resource.
   *
   * @param $endpoint_url
   *   Resource specific URL of the oEmbed endpoint.
   *
   * @return array|false
   *   Resource information as returned from the oEmbed endpoint or FALSE if
   *   the resource could not be fetched.
   */
  public function fetchResource($endpoint_url);

}
