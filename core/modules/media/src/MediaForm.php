<?php

namespace Drupal\media;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\EventSubscriber\MainContentViewSubscriber;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\media\Ajax\OpenUrlCommand;

/**
 * Form controller for the media edit forms.
 */
class MediaForm extends ContentEntityForm {

  /**
   * Default settings for this media bundle.
   *
   * @var array
   */
  protected $settings;

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\media\MediaInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  protected function prepareEntity() {
    parent::prepareEntity();

    $fids = $this->getRequest()->query->get('fids');
    if ($fids) {
      // @todo: Fix file upload location.
      $file = $this->entityTypeManager
        ->getStorage('file')
        ->load($fids[0]);

      $handler = $this->entity->getHandler();
      if ($handler instanceof SourceFieldInterface) {
        $this->entity->set($handler->getSourceField($this->entity->bundle->entity)->getName(), $file);
      }
    }

    // Set up default values, if required.
    if (!$this->getEntity()->isNew()) {
      $this->entity->setOwnerId($this->currentUser()->id());
      $this->entity->setCreatedTime($this->time->getRequestTime());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\media\MediaTypeInterface $bundle_entity */
    $bundle_entity = $this->entity->bundle->entity;

    if ($this->operation == 'edit') {
      $form['#title'] = $this->t('Edit %bundle_label @label', [
        '%bundle_label' => $bundle_entity->label(),
        '@label' => $this->entity->label(),
      ]);
    }

    // Media author information for administrators.
    if (isset($form['uid']) || isset($form['created'])) {
      $form['author'] = [
        '#type' => 'details',
        '#title' => $this->t('Authoring information'),
        '#group' => 'advanced',
        '#attributes' => [
          'class' => ['media-form-author'],
        ],
        '#weight' => 90,
        '#optional' => TRUE,
      ];
    }

    if (isset($form['uid'])) {
      $form['uid']['#group'] = 'author';
    }

    if (isset($form['created'])) {
      $form['created']['#group'] = 'author';
    }

    $form['#attached']['library'][] = 'media/media_form';

    $form['#entity_builders']['update_status'] = [$this, 'updateStatus'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $element = parent::actions($form, $form_state);
    $media = $this->entity;

    // Add extra submit handler for ajax requests.
    $element['submit']['#submit'][] = '::afterSave';
    $element['submit']['#ajax'] = [
      'callback' => '::ajaxModalSubmit',
    ];

    // Add a "Publish" button.
    $element['publish'] = $element['submit'];
    // If the "Publish" button is clicked, we want to update the status to
    // "published".
    $element['publish']['#published_status'] = TRUE;
    $element['publish']['#dropbutton'] = 'save';
    if ($media->isNew()) {
      $element['publish']['#value'] = $this->t('Save and publish');
    }
    else {
      $element['publish']['#value'] = $media->isPublished() ? $this->t('Save and keep published') : $this->t('Save and publish');
    }
    $element['publish']['#weight'] = 0;

    // Add a "Unpublish" button.
    $element['unpublish'] = $element['submit'];
    // If the "Unpublish" button is clicked, we want to update the status to
    // "unpublished".
    $element['unpublish']['#published_status'] = FALSE;
    $element['unpublish']['#dropbutton'] = 'save';
    if ($media->isNew()) {
      $element['unpublish']['#value'] = $this->t('Save as unpublished');
    }
    else {
      $element['unpublish']['#value'] = !$media->isPublished() ? $this->t('Save and keep unpublished') : $this->t('Save and unpublish');
    }
    $element['unpublish']['#weight'] = 10;

    // If already published, the 'publish' button is primary.
    if ($media->isPublished()) {
      unset($element['unpublish']['#button_type']);
    }
    // Otherwise, the 'unpublish' button is primary and should come first.
    else {
      unset($element['publish']['#button_type']);
      $element['unpublish']['#weight'] = -10;
    }

    // Remove the "Save" button.
    $element['submit']['#access'] = FALSE;

    $element['delete']['#access'] = $media->access('delete');
    $element['delete']['#weight'] = 100;

    unset($element['publish']['#dropbutton']);
    unset($element['unpublish']['#dropbutton']);

    return $element;
  }

  /**
   * Entity builder updating the media status with the submitted value.
   *
   * @param string $entity_type_id
   *   The entity type identifier.
   * @param \Drupal\media\MediaInterface $media
   *   The media updated with the submitted values.
   * @param array $form
   *   The complete form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see \Drupal\media\MediaForm::form()
   */
  public function updateStatus($entity_type_id, MediaInterface $media, array $form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
    if (!empty($element['#published_status'])) {
      $media->setPublished();
    }
    else {
      $media->setUnpublished();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $saved = parent::save($form, $form_state);
    $context = ['@type' => $this->entity->bundle(), '%label' => $this->entity->label()];
    $logger = $this->logger('media');
    $t_args = ['@type' => $this->entity->bundle->entity->label(), '%label' => $this->entity->label()];

    if ($saved === SAVED_NEW) {
      $logger->notice('@type: added %label.', $context);
      drupal_set_message($this->t('@type %label has been created.', $t_args));
    }
    else {
      $logger->notice('@type: updated %label.', $context);
      drupal_set_message($this->t('@type %label has been updated.', $t_args));
    }

    $form_state->setRedirectUrl($this->entity->toUrl('canonical'));
    return $saved;
  }

  /**
   * {@inheritdoc}
   */
  public function afterSave(array &$form, FormStateInterface $form_state) {
    $form_state->disableRedirect();
    $form_state->setValue('entity_id', $this->entity->id());
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxModalSubmit(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    $field_id = $this->getRequest()->query->get('field_id');
    $fids = array_slice($this->getRequest()->query->get('fids'), 1);
    $entity_id = $form_state->getValue('entity_id');

    // Check if we have a saved entity.
    if ($entity_id) {
      if ($field_id) {
        $field_name = $this->entityTypeManager
          ->getStorage('field_config')
          ->load($field_id)
          ->getName();

        // Pass id back to field widget and trigger form update.
        $input_selector = '[data-media-file-input-id="' . $field_name . '"]';
        $submit_selector = '[data-media-file-submit-id="' . $field_name . '"]';
        $response->addCommand(new InvokeCommand($input_selector, 'val', [$entity_id]));
        $response->addCommand(new InvokeCommand($submit_selector, 'trigger', ['mousedown']));
      }
    }

    // If we still have file IDs, return to media add list for next file,
    // or close the modal if all files are added.
    if ($fids) {
      $url = Url::fromRoute('entity.media.add_page', [], [
        'query' => [
          'field_id' => $field_id,
          'fids' => $fids,
        ],
      ]);
      $response->addCommand(new OpenUrlCommand($url));
    }
    else {
      $response->addCommand(new CloseModalDialogCommand());
    }

    return $response;
  }

}
