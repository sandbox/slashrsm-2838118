<?php

namespace Drupal\media\Form;

use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for media type deletion.
 */
class MediaTypeDeleteConfirmForm extends EntityDeleteForm {

  /**
   * The query factory to create entity queries.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $queryFactory;

  /**
   * Constructs a new MediaTypeDeleteConfirm object.
   *
   * @param \Drupal\Core\Entity\Query\QueryFactory $query_factory
   *   The entity query object.
   */
  public function __construct(QueryFactory $query_factory) {
    $this->queryFactory = $query_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.query')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $num_entities = $this->queryFactory->get('media')
      ->condition('bundle', $this->entity->id())
      ->count()
      ->execute();
    if ($num_entities) {
      $form['#title'] = $this->getQuestion();
      $form['description'] = [
        '#type' => 'inline_template',
        '#template' => '<p>{{ message }}</p>',
        '#context' => [
          'message' => $this->formatPlural($num_entities,
            '%type is used by @count piece of content on your site. You can not remove this content type until you have removed all of the %type content.',
            '%type is used by @count pieces of content on your site. You may not remove %type until you have removed all of the %type content.',
            ['%type' => $this->entity->label()]),
        ],
      ];

      return $form;
    }

    return parent::buildForm($form, $form_state);
  }

}
