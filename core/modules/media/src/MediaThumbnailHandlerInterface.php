<?php

namespace Drupal\media;

/**
 * Provides an interface defining a media thumbnail service.
 */
interface MediaThumbnailHandlerInterface {

  /**
   * Sets the media thumbnail.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The Media entity.
   */
  public function setThumbnail(MediaInterface $media);

}
