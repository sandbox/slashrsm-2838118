<?php

namespace Drupal\media\Ajax;

use Drupal\Core\Ajax\CommandInterface;
use Drupal\Core\Url;

/**
 * AJAX command to open a URL in a modal dialog.
 */
class OpenUrlCommand implements CommandInterface {

  /**
   * The URL to open.
   *
   * @var \Drupal\Core\GeneratedUrl|string
   */
  protected $url;

  /**
   * OpenUrlCommand constructor.
   *
   * @param \Drupal\Core\Url|string $url
   *   The URL to open.
   */
  public function __construct($url) {
    $this->url = $url instanceof Url ? $url->toString() : $url;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'open_url',
      'url' => $this->url,
    ];
  }

}
