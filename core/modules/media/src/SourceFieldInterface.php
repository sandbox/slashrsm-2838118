<?php

namespace Drupal\media;

/**
 * Interface for media handler plugins that depend on a field.
 */
interface SourceFieldInterface extends MediaHandlerInterface {

  /**
   * Returns the source field for a bundle using this plugin.
   *
   * @param \Drupal\media\MediaTypeInterface $type
   *   The media bundle.
   *
   * @return \Drupal\field\FieldConfigInterface
   *   The source field definition.
   */
  public function getSourceField(MediaTypeInterface $type);

  /**
   * Gets source value.
   *
   * Value that is stored in the source field.
   *
   * @param MediaInterface $media
   *   The media entity class.
   *
   * @return \Drupal\Core\TypedData\TypedDataInterface
   *   The source value.
   */
  public function getSourceValue(MediaInterface $media);

}
