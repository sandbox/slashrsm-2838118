<?php

namespace Drupal\media;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\media\Entity\MediaType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for media type forms.
 */
class MediaTypeForm extends EntityForm {

  /**
   * Media handler plugin manager.
   *
   * @var \Drupal\media\MediaHandlerManager
   */
  protected $handlerManager;

  /**
   * Entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity being created or modified.
   *
   * @var \Drupal\media\MediaTypeInterface
   */
  protected $entity;

  /**
   * Constructs a new class instance.
   *
   * @param \Drupal\media\MediaHandlerManager $handler_manager
   *   Media handler plugin manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   */
  public function __construct(MediaHandlerManager $handler_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->handlerManager = $handler_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.media.handler'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * Ajax callback triggered by the type provider select element.
   */
  public function ajaxHandlerData(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#handler-dependent', $form['handler_dependent']));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    // Handler is not set when the entity is initially created.
    /** @var \Drupal\media\MediaHandlerInterface $handler */
    $handler = $this->entity->get('handler') ? $this->entity->getHandler() : NULL;

    if ($this->operation === 'add') {
      $form['#title'] = $this->t('Add media type');
    }

    $form['label'] = [
      '#title' => $this->t('Name'),
      '#type' => 'textfield',
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('The human-readable name of this media type.'),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#maxlength' => 32,
      '#disabled' => !$this->entity->isNew(),
      '#machine_name' => [
        'exists' => [MediaType::class, 'load'],
      ],
      '#description' => $this->t('A unique machine-readable name for this media type.'),
    ];

    $form['description'] = [
      '#title' => $this->t('Description'),
      '#type' => 'textarea',
      '#default_value' => $this->entity->getDescription(),
      '#description' => $this->t('Describe this media type. The text will be displayed on the <em>Add new media</em> page.'),
    ];

    $plugins = $this->handlerManager->getDefinitions();
    $options = [];
    foreach ($plugins as $plugin_id => $definition) {
      $options[$plugin_id] = $definition['label'];
    }

    $form['handler_dependent'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'handler-dependent'],
    ];

    $form['handler_dependent']['handler'] = [
      '#type' => 'select',
      '#title' => $this->t('Handler'),
      '#default_value' => $handler ? $handler->getPluginId() : NULL,
      '#options' => $options,
      '#description' => $this->t('Media handler that is responsible for additional logic related to this media type.'),
      '#ajax' => ['callback' => '::ajaxHandlerData'],
      '#required' => TRUE,
    ];

    if (!$handler) {
      $form['type']['#empty_option'] = $this->t('- Select handler -');
    }

    if ($handler) {
      // Media handler plugin configuration.
      $form['handler_dependent']['handler_configuration'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Handler configuration'),
        '#tree' => TRUE,
      ];

      $form['handler_dependent']['handler_configuration'] = $handler->buildConfigurationForm($form['handler_dependent']['handler_configuration'], $this->getHandlerSubFormState($form, $form_state));
    }

    // Field mapping configuration.
    $form['handler_dependent']['field_map'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Field mapping'),
      '#tree' => TRUE,
      'description' => [
        '#markup' => '<p>' . $this->t('Media handlers can provide metadata fields such as title, caption, size information, credits, etc. Media can automatically save this metadata information to entity fields, which can be configured below. Information will only be mapped if the entity field is empty.') . '</p>',
      ],
    ];

    if (empty($handler) || empty($handler->getProvidedFields())) {
      $form['handler_dependent']['field_map']['#access'] = FALSE;
    }
    else {
      $options = ['_none' => $this->t('- Skip field -')];
      foreach ($this->entityFieldManager->getFieldDefinitions('media', $this->entity->id()) as $field_name => $field) {
        if (!($field instanceof BaseFieldDefinition) || $field_name == 'name') {
          $options[$field_name] = $field->getLabel();
        }
      }

      $field_map = $this->entity->getFieldMap();
      foreach ($handler->getProvidedFields() as $field_name => $field_definition) {
        $form['handler_dependent']['field_map'][$field_name] = [
          '#type' => 'select',
          '#title' => $field_definition['label'],
          '#options' => $options,
          '#default_value' => isset($field_map[$field_name]) ? $field_map[$field_name] : '_none',
        ];
      }
    }

    $form['additional_settings'] = [
      '#type' => 'vertical_tabs',
      '#attached' => [
        'library' => ['media/media_type_form'],
      ],
    ];

    $form['workflow'] = [
      '#type' => 'details',
      '#title' => $this->t('Publishing options'),
      '#group' => 'additional_settings',
    ];

    $form['workflow']['options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Default options'),
      '#default_value' => $this->getWorkflowOptions(),
      '#options' => [
        'status' => $this->t('Published'),
        'new_revision' => $this->t('Create new revision'),
        'queue_thumbnail_downloads' => $this->t('Queue thumbnail downloads'),
      ],
    ];

    $form['workflow']['options']['status']['#description'] = $this->t('Media will be automatically published when created.');
    $form['workflow']['options']['new_revision']['#description'] = $this->t('Automatically create new revisions. Users with the Administer media permission will be able to override this option.');
    $form['workflow']['options']['queue_thumbnail_downloads']['#description'] = $this->t('Download thumbnails via a queue.');

    if ($this->moduleHandler->moduleExists('language')) {
      $form['language'] = [
        '#type' => 'details',
        '#title' => $this->t('Language settings'),
        '#group' => 'additional_settings',
      ];

      $language_configuration = call_user_func(
        ['\Drupal\language\Entity\ContentLanguageSettings', 'loadByEntityTypeBundle'],
        'media',
        $this->entity->id()
      );

      $form['language']['language_configuration'] = [
        '#type' => 'language_configuration',
        '#entity_information' => [
          'entity_type' => 'media',
          'bundle' => $this->entity->id(),
        ],
        '#default_value' => $language_configuration,
      ];
    }

    return $form;
  }

  /**
   * Prepares workflow options to be used in the 'checkboxes' form element.
   *
   * @return array
   *   Array of options ready to be used in #options.
   */
  protected function getWorkflowOptions() {
    $workflow_options = [
      'status' => $this->entity->getStatus(),
      'new_revision' => $this->entity->shouldCreateNewRevision(),
      'queue_thumbnail_downloads' => $this->entity->getQueueThumbnailDownloadsStatus(),
    ];
    // Prepare workflow options to be used for 'checkboxes' form element.
    $keys = array_keys(array_filter($workflow_options));
    return array_combine($keys, $keys);
  }

  /**
   * Gets sub-form state for the handler configuration sub-form.
   *
   * @param array $form
   *   Full form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Parent form state.
   *
   * @return \Drupal\Core\Form\SubFormStateInterface
   *   Sub-form state for the handler configuration form.
   */
  protected function getHandlerSubFormState(array $form, FormStateInterface $form_state) {
    return SubformState::createForSubform($form['handler_dependent']['handler_configuration'], $form, $form_state)
      ->set('operation', $this->operation)
      ->set('type', $this->entity);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if ($form['handler_dependent']['handler_configuration']) {
      // Let the selected plugin validate its settings.
      $this->entity->getHandler()->validateConfigurationForm($form['handler_dependent']['handler_configuration'], $this->getHandlerSubFormState($form, $form_state));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setValue('field_map', array_filter(
      $form_state->getValue('field_map', []),
      function ($item) {
        return $item != '_none';
      }
    ));

    parent::submitForm($form, $form_state);

    $this->entity->setQueueThumbnailDownloadsStatus((bool) $form_state->getValue(['options', 'queue_thumbnail_downloads']));
    $this->entity->setStatus((bool) $form_state->getValue(['options', 'status']));
    $this->entity->setNewRevision((bool) $form_state->getValue(['options', 'new_revision']));

    if ($form['handler_dependent']['handler_configuration']) {
      // Let the selected plugin save its settings.
      $this->entity->getHandler()->submitConfigurationForm($form['handler_dependent']['handler_configuration'], $this->getHandlerSubFormState($form, $form_state));
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save');
    $actions['delete']['#value'] = $this->t('Delete');
    $actions['delete']['#access'] = $this->entity->access('delete');
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $this->entity->set('auto_create_source_field', TRUE);
    $status = parent::save($form, $form_state);
    $bundle = $this->entity;

    $t_args = ['%name' => $bundle->label()];
    if ($status == SAVED_UPDATED) {
      drupal_set_message($this->t('The media type %name has been updated.', $t_args));
    }
    elseif ($status == SAVED_NEW) {
      drupal_set_message($this->t('The media type %name has been added.', $t_args));
      $this->logger('media')->notice('Added type %name.', $t_args);
    }

    // Override the "status" base field default value, for this bundle.
    $fields = $this->entityFieldManager->getFieldDefinitions('media', $bundle->id());
    /** @var \Drupal\media\MediaInterface $media */
    $media = $this->entityTypeManager->getStorage('media')->create(['bundle' => $bundle->id()]);
    $value = (bool) $form_state->getValue(['options', 'status']);
    if ($media->status->value != $value) {
      $fields['status']->getConfig($bundle->id())->setDefaultValue($value)->save();
    }

    $form_state->setRedirectUrl($bundle->toUrl('collection'));
  }

}
