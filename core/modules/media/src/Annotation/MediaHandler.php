<?php

namespace Drupal\media\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a media handler plugin annotation object.
 *
 * Media handlers are responsible for implementing all the logic for dealing
 * with a particular type of media asset. They provide various universal and
 * type-specific metadata about media assets of the type they handle.
 *
 * Plugin namespace: Plugin\media\Handler
 *
 * For a working example, see \Drupal\media\Plugin\media\Handler\File.
 *
 * @see \Drupal\media\MediaHandlerInterface
 * @see \Drupal\media\MediaHandlerBase
 * @see \Drupal\media\MediaHandlerManager
 * @see hook_media_handler_info_alter()
 * @see plugin_api
 *
 * @Annotation
 */
class MediaHandler extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the handler.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A brief description of the plugin.
   *
   * This will be shown when adding or configuring this display.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description = '';

  /**
   * The field types that can be used as a source field for this handler.
   *
   * @var string[]
   */
  public $allowed_field_types = [];

}
